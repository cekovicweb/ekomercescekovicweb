using AutoMapper;
using Ekomerce.API.Dtos;
using Ekomerce.API.Models;

namespace Ekomerce.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>(); 
            CreateMap<User, UserForDetailedDto>(); 
        }
    }
}