using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Dtos
{
    public class UserForRegisterDto
    {
        [Required(ErrorMessage="Nazwa użytkownika jest wymagana")]
        public string Username { get; set; }
        [Required(ErrorMessage="Hasło jest wymagane")]
        [StringLength(12, MinimumLength=6, ErrorMessage="Hasło musi się składać z 6 do 12 znaków")]
        public string Password { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public System.DateTime DateOfBirth { get; set; }
        public string ZodiacSign { get; set; }
        public System.DateTime Created { get; set; }
        public System.DateTime LastActive { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string AccountType { get; set; }
    }
}