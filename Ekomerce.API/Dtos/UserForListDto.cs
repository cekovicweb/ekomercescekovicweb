using System;
using System.Collections.Generic;
using Ekomerce.API.Models;

namespace Ekomerce.API.Dtos
{
    public class UserForListDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }


        // Postawowe informacje
        public string Gender { get; set; }          // Płeć
        // public int Age { get; set; }                // Wiek
        public string ZodiacSign { get; set; }      // Znak zodiaku
        public DateTime Created { get; set; }       // Data utworzenia/rejestracji
        public DateTime LastActive { get; set; }    // Ostatnia aktywność
        public string City { get; set; }            // Miasto
        public string Country { get; set; }         // Kraj
        public string AccountType { get; set; }         // Kraj
        // public string PhotoUrl { get; set; }
        public virtual ICollection<RateUser> RateUsers { get; set; }
        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }
        public int? CoachId { get; set; }
        public int? CoachPackageId { get; set; }
        public virtual ICollection<DietPlan> DietPlans { get; set; }
        public virtual ICollection<TrainingPlan> TrainingPlans { get; set; }
        public int? PhysiotherapistId { get; set; }
        public int? PhysiotherapistPackageId { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public int? DoctorId { get; set; }
        public int? DoctorPackageId { get; set; }
        // public int? DietId { get; set; }
    }
}