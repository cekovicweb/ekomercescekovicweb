using System.Collections.Generic;
using System.Threading.Tasks;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;


namespace Ekomerce.API.Dtos
{
    public class UserRepository : GenericRepository, IUserRepository
    {
        private readonly DataContext _context;
        public UserRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users
                .Include(u => u.RateUsers)
                .Include(u => u.Image)
                .Include(d => d.DietPlans)
                    .ThenInclude(di => di.Diets)
                    .ThenInclude(p => p.DietProducts)
                .Include(tp => tp.TrainingPlans)
                    .ThenInclude(t => t.Trainings)
                    .ThenInclude(te => te.TrainingExercises)
                    .ThenInclude(i => i.Image)
                    .AsSingleQuery()
                .AsNoTracking()
                .FirstOrDefaultAsync(u => u.Id == id);
            return user;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            // var users = await _context.Users.Include(p => p.Name).ToListAsync();
            var users = await _context.Users.Include(u => u.RateUsers).Include(u => u.Image).AsSingleQuery()
                .AsNoTracking().ToListAsync();
            return users;
        }

    }
}