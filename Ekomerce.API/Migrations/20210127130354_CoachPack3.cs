﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ekomerce.API.Migrations
{
    public partial class CoachPack3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoachPackageUser");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "CoachPackages");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "CoachPackages",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CoachPackageUser",
                columns: table => new
                {
                    CoachPackagesId = table.Column<int>(type: "INTEGER", nullable: false),
                    UsersId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoachPackageUser", x => new { x.CoachPackagesId, x.UsersId });
                    table.ForeignKey(
                        name: "FK_CoachPackageUser_CoachPackages_CoachPackagesId",
                        column: x => x.CoachPackagesId,
                        principalTable: "CoachPackages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoachPackageUser_Users_UsersId",
                        column: x => x.UsersId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoachPackageUser_UsersId",
                table: "CoachPackageUser",
                column: "UsersId");
        }
    }
}
