﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ekomerce.API.Migrations
{
    public partial class PhysiotherapistEx : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PhysiotherapistsExs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Price = table.Column<string>(type: "TEXT", nullable: true),
                    PhysioPackageId = table.Column<int>(type: "INTEGER", nullable: true),
                    PhysiotherapistPackageId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhysiotherapistsExs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhysiotherapistsExs_PhysiotherapistPackages_PhysiotherapistPackageId",
                        column: x => x.PhysiotherapistPackageId,
                        principalTable: "PhysiotherapistPackages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PhysiotherapistsExs_PhysiotherapistPackageId",
                table: "PhysiotherapistsExs",
                column: "PhysiotherapistPackageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhysiotherapistsExs");
        }
    }
}
