﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ekomerce.API.Migrations
{
    public partial class Training2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImageId",
                table: "TrainingExercises",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TrainingExercises_ImageId",
                table: "TrainingExercises",
                column: "ImageId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingExercises_Images_ImageId",
                table: "TrainingExercises",
                column: "ImageId",
                principalTable: "Images",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainingExercises_Images_ImageId",
                table: "TrainingExercises");

            migrationBuilder.DropIndex(
                name: "IX_TrainingExercises_ImageId",
                table: "TrainingExercises");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "TrainingExercises");
        }
    }
}
