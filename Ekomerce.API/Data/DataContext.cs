using Ekomerce.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Ekomerce.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext>options) : base(options)
        {
        }

        public DbSet<Page> Pages { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        // protected override void OnModelCreating(ModelBuilder modelBuilder)
        // {
        //     modelBuilder.Entity<Product>()
        //     .HasOne(e => e.Category)
        //     .WithMany(c => c.Product);
        // }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<RateUser> RateUsers { get; set; }
        public DbSet<CoachPackage> CoachPackages { get; set; }
        public DbSet<Diet> Diets { get; set; }
        public DbSet<DietProduct> DietProducts { get; set; }
        public DbSet<DietPlan> DietPlans { get; set; }
        public DbSet<TrainingPlan> TrainingPlans { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<TrainingExercise> TrainingExercises { get; set; }
        public DbSet<PhysiotherapistPackage> PhysiotherapistPackages { get; set; }
        public DbSet<PhysiotherapistEx> PhysiotherapistsExs { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<DoctorPackage> DoctorPackages { get; set; }
        public DbSet<DoctorEx> DoctorExs { get; set; }
    }
}