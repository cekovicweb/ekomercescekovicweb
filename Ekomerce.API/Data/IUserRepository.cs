using System.Collections.Generic;
using System.Threading.Tasks;
using Ekomerce.API.Models;

namespace Ekomerce.API.Data
{
    public interface IUserRepository : IGenericRepository
    {
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(int id);

    }
}