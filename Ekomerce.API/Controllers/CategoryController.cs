using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly DataContext _context;

        public CategoryController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            var categorys = _context.Categorys.ToList();
            return Ok(categorys);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var category = _context.Categorys.FirstOrDefault(x => x.Id == id);
            return Ok(category);
        }

        [HttpPost]
        public IActionResult AddCategory([FromBody]Category categorys)
        {
            _context.Categorys.Add(categorys);
            _context.SaveChanges();
            return Ok(categorys);
        }

        [HttpPut("id")]
        public IActionResult EditCategory(int id, [FromBody]Category categorys)
        {
            var data = _context.Categorys.Find(id);

            //Modyfikacja kodu
            data.Id = categorys.Id;
            data.NameCategory = categorys.NameCategory;

            _context.Categorys.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("id")]
        public IActionResult DelateCategory(int id)
        {
            var delateCat = _context.Categorys.Find(id);

              if(delateCat == null)
                return NoContent();

            _context.Categorys.Remove(delateCat);
            _context.SaveChanges();

            return Ok(delateCat);
        }
    }
}