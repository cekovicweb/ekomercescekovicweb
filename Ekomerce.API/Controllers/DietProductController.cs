using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class DietProductController : ControllerBase
    {
        private readonly DataContext _context;

        public DietProductController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<DietProduct> dietProduct = _context.DietProducts.ToList();
            return Ok(dietProduct);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var dietProduct = _context.DietProducts.FirstOrDefault(x => x.Id == id);
            return Ok(dietProduct);
        }

        [HttpPost]
        public IActionResult Add([FromBody]DietProduct dietProduct)
        {
            _context.DietProducts.Add(dietProduct);
            _context.SaveChanges();
            return Ok(dietProduct);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]DietProduct dietProduct)
        {
            var data = _context.DietProducts.Find(id);

            //Modyfikacja kodu
            data.Id = dietProduct.Id;
            data.ProductName = dietProduct.ProductName;
            data.Weight = dietProduct.Weight;
            data.Price = dietProduct.Price;
            data.PriceForWeight = dietProduct.PriceForWeight;
            data.DietId = dietProduct.DietId;

            _context.DietProducts.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var delete = _context.DietProducts.Find(id);

              if(delete == null)
                return NoContent();

            _context.DietProducts.Remove(delete);
            _context.SaveChanges();

            return Ok(delete);
        }
    }
}