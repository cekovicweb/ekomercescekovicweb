using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly DataContext _context;

        public ServiceController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<Service> service = _context.Services.Include(a => a.User).ToList();
            return Ok(service);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var service = _context.Services.Include(a => a.User).FirstOrDefault(x => x.Id == id);
            return Ok(service);
        }

        [HttpPost]
        public IActionResult AddService([FromBody]Service services)
        {
            _context.Services.Add(services);
            _context.SaveChanges();
            return Ok(services);
        }

        [HttpPut("{id}")]
        public IActionResult EditService(int id, [FromBody]Service service)
        {
            var data = _context.Services.Find(id);

            //Modyfikacja kodu
            data.ServiceName = service.ServiceName;
            data.Price = service.Price;
            data.UserId = service.UserId;

            _context.Services.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult DelateService(int id)
        {
            var delateArt = _context.Services.Find(id);

              if(delateArt == null)
                return NoContent();

            _context.Services.Remove(delateArt);
            _context.SaveChanges();

            return Ok(delateArt);
        }
    }
}