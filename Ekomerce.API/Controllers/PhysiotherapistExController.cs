using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class PhysiotherapistExController : ControllerBase
    {
        private readonly DataContext _context;

        public PhysiotherapistExController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
             IList<PhysiotherapistEx> physiotherapistEx = _context.PhysiotherapistsExs.ToList();
            return Ok(physiotherapistEx);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var physiotherapistEx = _context.PhysiotherapistsExs.FirstOrDefault(x => x.Id == id);
            return Ok(physiotherapistEx);
        }

        [HttpPost]
        public IActionResult Add([FromBody]PhysiotherapistEx physiotherapistEx)
        {
            _context.PhysiotherapistsExs.Add(physiotherapistEx);
            _context.SaveChanges();
            return Ok(physiotherapistEx);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]PhysiotherapistEx physiotherapistEx)
        {
            var data = _context.PhysiotherapistsExs.Find(id);

            //Modyfikacja kodu
            data.Name = physiotherapistEx.Name;
            data.Price = physiotherapistEx.Price;
            data.PhysiotherapistPackageId = physiotherapistEx.PhysiotherapistPackageId;

            _context.PhysiotherapistsExs.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delate(int id)
        {
            var delate = _context.PhysiotherapistsExs.Find(id);

              if(delate == null)
                return NoContent();

            _context.PhysiotherapistsExs.Remove(delate);
            _context.SaveChanges();

            return Ok(delate);
        }
    }
}