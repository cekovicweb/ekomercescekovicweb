using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingExerciseController : ControllerBase
    {
        private readonly DataContext _context;

        public TrainingExerciseController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<TrainingExercise> trainingExercise = _context.TrainingExercises.Include(i => i.Image).ToList();
            return Ok(trainingExercise);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var trainingExercise = _context.TrainingExercises.Include(i => i.Image).FirstOrDefault(x => x.Id == id);
            return Ok(trainingExercise);
        }

        [HttpPost]
        public IActionResult Add([FromBody]TrainingExercise trainingExercise)
        {
            _context.TrainingExercises.Add(trainingExercise);
            _context.SaveChanges();
            GetValues();
            return Ok(trainingExercise);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]TrainingExercise trainingExercise)
        {
            var data = _context.TrainingExercises.Find(id);

            //Modyfikacja kodu
            data.Id = trainingExercise.Id;
            data.ExerciseName = trainingExercise.ExerciseName;
            data.Duration = trainingExercise.Duration;
            data.Reapeat = trainingExercise.Reapeat;
            data.TrainingId = trainingExercise.TrainingId;
            data.ImageId = trainingExercise.ImageId;

            _context.TrainingExercises.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var delete = _context.TrainingExercises.Find(id);

              if(delete == null)
                return NoContent();

            _context.TrainingExercises.Remove(delete);
            _context.SaveChanges();

            return Ok(delete);
        }
    }
}