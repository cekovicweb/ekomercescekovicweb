using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Net.Http.Headers;
using System;
using System.Threading.Tasks;

namespace Ekomerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly DataContext _context;

        public ProductController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetValues()
        {
            // var product = _context.Products.ToList();
            IEnumerable<Product> product = await _context.Products.Include(p => p.Category).Include(a => a.User).Include(i => i.Image).AsSingleQuery().AsNoTracking().ToListAsync();
            return Ok(product);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var product = _context.Products.Include(p => p.Category).Include(a => a.User).Include(i => i.Image).AsSingleQuery().AsNoTracking().FirstOrDefault(x => x.Id == id);
            return Ok(product);
        }

        [HttpPost]
        public IActionResult AddProduct([FromBody]Product products)
        {

            _context.Products.Add(products);
            _context.SaveChanges();
            return Ok(products);
        }

        [HttpPut("{id}")]
        public IActionResult EditProduct(int id, [FromBody]Product product)
        {
            var data = _context.Products.Find(id);
            data.Name = product.Name;
            data.Description = product.Description;
            data.Price = product.Price;
            data.IsMain = product.IsMain;
            data.Cart = product.Cart;
            data.CategoryId = product.CategoryId;
            data.UserId = product.UserId;
            data.ImageId = product.ImageId;

            _context.Products.Update(data);
            _context.SaveChanges();
            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult DelatePage(int id)
        {
            var delatePage = _context.Products.Find(id);

            if(delatePage == null)
                return NoContent();

            _context.Products.Remove(delatePage);
            _context.SaveChanges();
            return Ok(delatePage);
        }

    }
}