using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class PhysiotherapistPackageController : ControllerBase
    {
        private readonly DataContext _context;

        public PhysiotherapistPackageController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
             IList<PhysiotherapistPackage> physiotherapistPackage = _context.PhysiotherapistPackages.Include(e => e.PhysiotherapistsExs).ToList();
            return Ok(physiotherapistPackage);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var physiotherapistPackage = _context.PhysiotherapistPackages.Include(e => e.PhysiotherapistsExs).FirstOrDefault(x => x.Id == id);
            return Ok(physiotherapistPackage);
        }

        [HttpPost]
        public IActionResult Add([FromBody]PhysiotherapistPackage physiotherapistPackage)
        {
            _context.PhysiotherapistPackages.Add(physiotherapistPackage);
            _context.SaveChanges();
            return Ok(physiotherapistPackage);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]PhysiotherapistPackage physiotherapistPackage)
        {
            var data = _context.PhysiotherapistPackages.Find(id);

            //Modyfikacja kodu
            data.PackageName = physiotherapistPackage.PackageName;
            data.Price = physiotherapistPackage.Price;
            data.PhysiotherapistId = physiotherapistPackage.PhysiotherapistId;

            _context.PhysiotherapistPackages.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        // [HttpDelete("{id}")]
        // public IActionResult DelateService(int id)
        // {
        //     var delateArt = _context.Services.Find(id);

        //       if(delateArt == null)
        //         return NoContent();

        //     _context.Services.Remove(delateArt);
        //     _context.SaveChanges();

        //     return Ok(delateArt);
        // }
    }
}