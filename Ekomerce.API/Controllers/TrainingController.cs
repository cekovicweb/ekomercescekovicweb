using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingController : ControllerBase
    {
        private readonly DataContext _context;

        public TrainingController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<Training> training = _context.Trainings.Include(te => te.TrainingExercises).ThenInclude(i => i.Image).ToList();
            return Ok(training);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var training = _context.Trainings.Include(te => te.TrainingExercises).ThenInclude(i => i.Image).FirstOrDefault(x => x.Id == id);
            return Ok(training);
        }

        [HttpPost]
        public IActionResult Add([FromBody]Training training)
        {
            _context.Trainings.Add(training);
            _context.SaveChanges();
            return Ok(training);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]Training training)
        {
            var data = _context.Trainings.Find(id);

            //Modyfikacja kodu
            data.Id = training.Id;
            data.TrainingName = training.TrainingName;
            data.Calorific = training.Calorific;
            data.Duration = training.Duration;
            data.TrainingPlanId = training.TrainingPlanId;

            _context.Trainings.Update(data);
            _context.SaveChanges();
            GetValues();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var delete = _context.Trainings.Find(id);

              if(delete == null)
                return NoContent();

            _context.Trainings.Remove(delete);
            _context.SaveChanges();

            return Ok(delete);
        }
    }
}