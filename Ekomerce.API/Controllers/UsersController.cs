using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Ekomerce.API.Data;
using Ekomerce.API.Dtos;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Ekomerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _repo;
        private readonly IMapper _mapper;

        private readonly DataContext _context;

        public UsersController(IUserRepository repo, IMapper mapper, DataContext context)
        {
            _repo = repo;
            _mapper = mapper;
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {         
            var users = await _repo.GetUsers();

            var usersToReturn = _mapper.Map<IEnumerable<UserForListDto>>(users);

            return Ok(usersToReturn);
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user = await _repo.GetUser(id);

            var userToReturn = _mapper.Map<UserForDetailedDto>(user);

            return Ok(userToReturn);
        }
        
        [HttpPut("{id}")]
        public IActionResult UpdateUser(int id, [FromBody]User user)
        {
            var data = _context.Users.Find(id);
            data.CoachId = user.CoachId;
            data.CoachPackageId = user.CoachPackageId;

            data.PhysiotherapistId = user.PhysiotherapistId;
            data.PhysiotherapistPackageId = user.PhysiotherapistPackageId;
            data.DoctorId = user.DoctorId;
            data.DoctorPackageId = user.DoctorPackageId;

            data.ImageId = user.ImageId;

            _context.Users.Update(data);
            _context.SaveChanges();
            return Ok(data);
        }
    }
}