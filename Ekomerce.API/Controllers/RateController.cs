using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class RateController : ControllerBase
    {
        private readonly DataContext _context;

        public RateController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var articles = _context.Articles.ToList();
            IList<Rate> rate = _context.Rates.Include(a => a.User).Include(a => a.Article).ToList();
            return Ok(rate);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var rate = _context.Rates.Include(a => a.User).Include(a => a.Article).FirstOrDefault(x => x.Id == id);
            return Ok(rate);
        }

        [HttpPost]
        public IActionResult AddRate([FromBody]Rate rates)
        {
            _context.Rates.Add(rates);
            _context.SaveChanges();
            return Ok(rates);
        }

    //     [HttpPut("{id}")]
    //     public IActionResult EditArticle(int id, [FromBody]Article article)
    //     {
    //         var data = _context.Articles.Find(id);

    //         //Modyfikacja kodu
    //         data.Title  = article.Title;
    //         data.ShortDesc = article.ShortDesc;
    //         data.LongDesc = article.LongDesc;
    //         data.ShowTime = article.ShowTime;
    //         data.Date = article.Date;
    //         data.Time = article.Time;
    //         data.UserId = article.UserId;
    //         data.ImageId = article.ImageId;
            

    //         _context.Articles.Update(data);
    //         _context.SaveChanges();

    //         return Ok(data);
    //     }

    //     [HttpDelete("{id}")]
    //     public IActionResult DelateArticle(int id)
    //     {
    //         var delateArt = _context.Articles.Find(id);

    //           if(delateArt == null)
    //             return NoContent();

    //         _context.Articles.Remove(delateArt);
    //         _context.SaveChanges();

    //         return Ok(delateArt);
    //     }
    }
}