using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly DataContext _context;

        public EventController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetValues()
        {
             IEnumerable<Event> events = await _context.Events.ToListAsync();
            return Ok(events);
        }

        [HttpGet("{id}")]
        public async Task<Event> GetValue(int id)
        {
            var ev = await _context.Events.FirstOrDefaultAsync(x => x.id == id);
            return ev;
        }

        [HttpPost]
        public IActionResult Add([FromBody]Event ev)
        {
            _context.Events.Add(ev);
            _context.SaveChanges();
            return Ok(ev);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]Event ev)
        {
            var data = _context.Events.Find(id);

            //Modyfikacja kodu
            data.start = ev.start;
            data.end = ev.end;
            data.title = ev.title;
            data.primary = ev.primary;
            data.secondary = ev.secondary;

            _context.Events.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var delete = _context.Events.Find(id);

              if(delete == null)
                return NoContent();

            _context.Events.Remove(delete);
            _context.SaveChanges();

            return Ok(delete);
        }
    }
}