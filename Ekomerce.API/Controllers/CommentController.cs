using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly DataContext _context;

        public CommentController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<Comment> comment = _context.Comments.Include(a => a.User).ToList();
            return Ok(comment);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var comment = _context.Comments.Include(a => a.User).FirstOrDefault(x => x.Id == id);
            return Ok(comment);
        }

        [HttpPost]
        public IActionResult AddComment([FromBody]Comment comments)
        {
            _context.Comments.Add(comments);
            _context.SaveChanges();
            return Ok(comments);
        }

        [HttpPut("{id}")]
        public IActionResult EditComment(int id, [FromBody]Comment comments)
        {
            var data = _context.Comments.Find(id);

            //Modyfikacja kodu
            data.Content = comments.Content;
            data.UserId = comments.UserId;
            data.ArticleId = comments.ArticleId;

            _context.Comments.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult DelateComment(int id)
        {
            var delete = _context.Comments.Find(id);

              if(delete == null)
                return NoContent();

            _context.Comments.Remove(delete);
            _context.SaveChanges();

            return Ok(delete);
        }
    }
}