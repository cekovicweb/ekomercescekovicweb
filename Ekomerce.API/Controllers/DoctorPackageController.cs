using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorPackageController : ControllerBase
    {
        private readonly DataContext _context;

        public DoctorPackageController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
             IList<DoctorPackage> doctorPackage = _context.DoctorPackages.Include(e => e.DoctorExs).ToList();
            return Ok(doctorPackage);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var doctorPackage = _context.DoctorPackages.Include(e => e.DoctorExs).FirstOrDefault(x => x.Id == id);
            return Ok(doctorPackage);
        }

        [HttpPost]
        public IActionResult Add([FromBody]DoctorPackage doctorPackage)
        {
            _context.DoctorPackages.Add(doctorPackage);
            _context.SaveChanges();
            return Ok(doctorPackage);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]DoctorPackage doctorPackage)
        {
            var data = _context.DoctorPackages.Find(id);

            //Modyfikacja kodu
            data.PackageName = doctorPackage.PackageName;
            data.Price = doctorPackage.Price;
            data.DoctorId = doctorPackage.DoctorId;

            _context.DoctorPackages.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        // [HttpDelete("{id}")]
        // public IActionResult DelateService(int id)
        // {
        //     var delateArt = _context.Services.Find(id);

        //       if(delateArt == null)
        //         return NoContent();

        //     _context.Services.Remove(delateArt);
        //     _context.SaveChanges();

        //     return Ok(delateArt);
        // }
    }
}