using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Ekomerce.API.Controllers
{
   // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class PageController : ControllerBase
    {
        private readonly DataContext _context;

        public PageController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            var page = _context.Pages.ToList();
            return Ok(page);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var page = _context.Pages.FirstOrDefault(x => x.Id == id);
            return Ok(page);
        }

        [HttpPost]
        public IActionResult AddPage([FromBody]Page pages)
        {
            _context.Pages.Add(pages);
            _context.SaveChanges();
            return Ok(pages);
        }

        [HttpPut("{id}")]
        public IActionResult EditPage(int id, [FromBody]Page pages)
        {
            var data = _context.Pages.Find(id);

            if(data == null)
                return NoContent();

            data.Title = pages.Title;
            data.Description = pages.Description;
            data.Author = pages.Author;
            data.URLPages = pages.URLPages;
            data.IsPrivate = pages.IsPrivate;

            _context.Pages.Update(data);
            _context.SaveChanges();
            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult DelatePage(int id)
        {
            var delatePage = _context.Pages.Find(id);

            if(delatePage == null)
                return NoContent();

            _context.Pages.Remove(delatePage);
            _context.SaveChanges();
            return Ok(delatePage);
        }
    }
}