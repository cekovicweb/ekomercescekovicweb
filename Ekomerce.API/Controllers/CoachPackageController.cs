using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class CoachPackageController : ControllerBase
    {
        private readonly DataContext _context;

        public CoachPackageController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<CoachPackage> coachPackage = _context.CoachPackages.ToList();
            return Ok(coachPackage);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var coachPackage = _context.CoachPackages.FirstOrDefault(x => x.Id == id);
            return Ok(coachPackage);
        }

        [HttpPost]
        public IActionResult Add([FromBody]CoachPackage coachPackage)
        {
            _context.CoachPackages.Add(coachPackage);
            _context.SaveChanges();
            return Ok(coachPackage);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]CoachPackage coachPackage)
        {
            var data = _context.CoachPackages.Find(id);

            //Modyfikacja kodu
            data.PackageName = coachPackage.PackageName;
            data.Price = coachPackage.Price;
            data.CoachId = coachPackage.CoachId;

            _context.CoachPackages.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        // [HttpDelete("{id}")]
        // public IActionResult DelateService(int id)
        // {
        //     var delateArt = _context.Services.Find(id);

        //       if(delateArt == null)
        //         return NoContent();

        //     _context.Services.Remove(delateArt);
        //     _context.SaveChanges();

        //     return Ok(delateArt);
        // }
    }
}