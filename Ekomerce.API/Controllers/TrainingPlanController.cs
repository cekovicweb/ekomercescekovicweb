using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingPlanController : ControllerBase
    {
        private readonly DataContext _context;

        public TrainingPlanController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<TrainingPlan> trainingPlan = _context.TrainingPlans.Include(t => t.Trainings).ThenInclude(te => te.TrainingExercises).ThenInclude(i => i.Image).ToList();
            return Ok(trainingPlan);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var trainingPlan = _context.TrainingPlans.Include(t => t.Trainings).ThenInclude(te => te.TrainingExercises).ThenInclude(i => i.Image).FirstOrDefault(x => x.Id == id);
            return Ok(trainingPlan);
        }

        [HttpPost]
        public IActionResult Add([FromBody]TrainingPlan trainingPlan)
        {
            _context.TrainingPlans.Add(trainingPlan);
            _context.SaveChanges();
            return Ok(trainingPlan);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]TrainingPlan trainingPlan)
        {
            var data = _context.TrainingPlans.Find(id);

            //Modyfikacja kodu
            data.Id = trainingPlan.Id;
            data.Time = trainingPlan.Time;
            data.UserId = trainingPlan.UserId;
            data.CoachId = trainingPlan.CoachId;

            _context.TrainingPlans.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var delete = _context.TrainingPlans.Find(id);

              if(delete == null)
                return NoContent();

            _context.TrainingPlans.Remove(delete);
            _context.SaveChanges();

            return Ok(delete);
        }
    }
}