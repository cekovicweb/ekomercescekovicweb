using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class DoctorExController : ControllerBase
    {
        private readonly DataContext _context;

        public DoctorExController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
             IList<DoctorEx> doctorEx = _context.DoctorExs.ToList();
            return Ok(doctorEx);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var doctorEx = _context.DoctorExs.FirstOrDefault(x => x.Id == id);
            return Ok(doctorEx);
        }

        [HttpPost]
        public IActionResult Add([FromBody]DoctorEx doctorEx)
        {
            _context.DoctorExs.Add(doctorEx);
            _context.SaveChanges();
            return Ok(doctorEx);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]DoctorEx doctorEx)
        {
            var data = _context.DoctorExs.Find(id);

            //Modyfikacja kodu
            data.Name = doctorEx.Name;
            data.Price = doctorEx.Price;
            data.DoctorPackageId = doctorEx.DoctorPackageId;

            _context.DoctorExs.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delate(int id)
        {
            var delate = _context.DoctorExs.Find(id);

              if(delate == null)
                return NoContent();

            _context.DoctorExs.Remove(delate);
            _context.SaveChanges();

            return Ok(delate);
        }
    }
}