using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;

namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly DataContext _context;

        public AccountController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            var acconuts = _context.Accounts.ToList();
            return Ok(acconuts);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(string id)
        {
            var account = _context.Accounts.FirstOrDefault(x => x.AccountName == id);
            return Ok(account);
        }

        [HttpPost]
        public IActionResult AddAccount([FromBody]Account accounts)
        {
            _context.Accounts.Add(accounts);
            _context.SaveChanges();
            return Ok(accounts);
        }

        [HttpPut("id")]
        public IActionResult EditAccount(int id, [FromBody]Account account)
        {
            var data = _context.Accounts.Find(id);

            //Modyfikacja kodu
            data.Id = account.Id;
            data.AccountName = account.AccountName;

            _context.Accounts.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("id")]
        public IActionResult DelateAccount(int id)
        {
            var delateAcc = _context.Accounts.Find(id);

              if(delateAcc == null)
                return NoContent();

            _context.Accounts.Remove(delateAcc);
            _context.SaveChanges();

            return Ok(delateAcc);
        }
    }
}