using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class DietController : ControllerBase
    {
        private readonly DataContext _context;

        public DietController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<Diet> diet = _context.Diets.Include(d => d.DietProducts).ToList();
            return Ok(diet);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var diet = _context.Diets.Include(d => d.DietProducts).FirstOrDefault(x => x.Id == id);
            return Ok(diet);
        }

        [HttpPost]
        public IActionResult Add([FromBody]Diet diet)
        {
            _context.Diets.Add(diet);
            _context.SaveChanges();
            return Ok(diet);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]Diet diet)
        {
            var data = _context.Diets.Find(id);

            //Modyfikacja kodu
            data.Id = diet.Id;
            data.DietName = diet.DietName;
            data.Calorific = diet.Calorific;
            data.DietPlanId = diet.DietPlanId;

            _context.Diets.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var delete = _context.Diets.Find(id);

              if(delete == null)
                return NoContent();

            _context.Diets.Remove(delete);
            _context.SaveChanges();

            return Ok(delete);
        }
    }
}