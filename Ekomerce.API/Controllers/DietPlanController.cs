using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;


namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class DietPlanController : ControllerBase
    {
        private readonly DataContext _context;

        public DietPlanController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetValues()
        {
            // var services = _context.Services.ToList();
             IList<DietPlan> dietPlan = _context.DietPlans.Include(d => d.Diets).ThenInclude(p => p.DietProducts).ToList();
            return Ok(dietPlan);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var dietPlan = _context.DietPlans.Include(d => d.Diets).ThenInclude(p => p.DietProducts).FirstOrDefault(x => x.Id == id);
            return Ok(dietPlan);
        }

        [HttpPost]
        public IActionResult Add([FromBody]DietPlan dietPlan)
        {
            _context.DietPlans.Add(dietPlan);
            _context.SaveChanges();
            return Ok(dietPlan);
        }

        [HttpPut("{id}")]
        public IActionResult Edit(int id, [FromBody]DietPlan dietPlan)
        {
            var data = _context.DietPlans.Find(id);

            //Modyfikacja kodu
            data.Id = dietPlan.Id;
            data.Time = dietPlan.Time;
            data.UserId = dietPlan.UserId;
            data.CoachId = dietPlan.CoachId;

            _context.DietPlans.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var delate = _context.DietPlans.Find(id);

              if(delate == null)
                return NoContent();

            _context.DietPlans.Remove(delate);
            _context.SaveChanges();

            return Ok(delate);
        }
    }
}