using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Ekomerce.API.Controllers
{
    // Post http://localhost:5000/api/
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly DataContext _context;

        public ArticleController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetValues()
        {
            // var articles = _context.Articles.ToList();
            IEnumerable<Article> article = await _context.Articles
                .Include(a => a.User)
                    .ThenInclude(o => o.RateUsers)
                .Include(a => a.User)
                    .ThenInclude(o => o.Image)
                .Include(a => a.Image)
                .Include(a => a.Rates)
                .Include(a => a.Comments)
                    .ThenInclude(o => o.User)
                .AsSingleQuery()
                .AsNoTracking()
                .ToListAsync();
            return Ok(article);
        }

        [HttpGet("{id}")]
        public IActionResult GetValue(int id)
        {
            var article = _context.Articles
                .Include(a => a.User)
                    .ThenInclude(o => o.RateUsers)
                .Include(a => a.Image)
                .Include(a => a.Rates)
                .Include(a => a.Comments)
                    .ThenInclude(o => o.User)
                .AsSingleQuery()
                .AsNoTracking()
                .FirstOrDefault(x => x.Id == id);
            return Ok(article);
        }

        [HttpPost]
        public IActionResult AddArticle([FromBody] Article articles)
        {
            _context.Articles.Add(articles);
            _context.SaveChanges();
            return Ok(articles);
        }

        [HttpPut("{id}")]
        public IActionResult EditArticle(int id, [FromBody] Article article)
        {
            var data = _context.Articles.Find(id);

            //Modyfikacja kodu
            data.Title = article.Title;
            data.ShortDesc = article.ShortDesc;
            data.LongDesc = article.LongDesc;
            data.ShowTime = article.ShowTime;
            data.Date = article.Date;
            data.Time = article.Time;
            data.UserId = article.UserId;
            data.ImageId = article.ImageId;


            _context.Articles.Update(data);
            _context.SaveChanges();

            return Ok(data);
        }

        [HttpDelete("{id}")]
        public IActionResult DelateArticle(int id)
        {
            var delateArt = _context.Articles.Find(id);

            if (delateArt == null)
                return NoContent();

            _context.Articles.Remove(delateArt);
            _context.SaveChanges();

            return Ok(delateArt);
        }
    }
}