using System.Linq;
using Ekomerce.API.Data;
using Ekomerce.API.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Net.Http.Headers;
using System;

namespace Ekomerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {


        private readonly DataContext _context;

        public ImageController(DataContext context)
        {
            _context = context;
        }

        [HttpPost, DisableRequestSizeLimit]
        public IActionResult UploadFile()
        {

            try
            {
                var file = Request.Form.Files[0];
                var folderName = Path.Combine("Resources", "Images");
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                if (file.Length > 0)
                {
                    var oldName =  ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var extension = Path.GetExtension(oldName);
                    // var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    var fileName = $@"{Guid.NewGuid()}{extension}";
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = "http://localhost:5000/" + Path.Combine(folderName, fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                    Image image = new Image();
                    image.Name = fileName;
                    image.Path = dbPath;
                    // var img = _context.Images.Find(image);
                    _context.Images.Add(image);
                    _context.SaveChanges();
                    return Ok(image);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex}");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DelateImage(int id)
        {
            var deleteImage = _context.Images.Find(id);
            var imageName = deleteImage.Name;
            var folderName = Path.Combine("Resources", "Images");
            var path = Path.Combine(folderName, imageName);

            if (deleteImage == null)
            {
                return NoContent();
            }
            else
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
                _context.Images.Remove(deleteImage);
                _context.SaveChanges();

                return Ok(deleteImage);
            }

            return BadRequest();
        }

    }
}