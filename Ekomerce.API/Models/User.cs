using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }  // Imię
        public string LastName { get; set; }   // Nazwisko
        public string Username { get; set; } // Nazwa użytkownika
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }

        public string Gender { get; set; }          // Płeć
        public DateTime DateOfBirth { get; set; }   // Data urodzenia
        public string ZodiacSign { get; set; }      // Znak zodiaku
        public DateTime Created { get; set; }       // Data utworzenia/rejestracji
        public DateTime LastActive { get; set; }    // Ostatnia aktywność
        public string City { get; set; }            // Miasto
        public string Country { get; set; }         // Kraj
        public string AccountType { get; set; }         // Kraj
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<Article> Articles { get; set; }
        public virtual ICollection<Service> Services { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
        public virtual ICollection<RateUser> RateUsers { get; set; }
        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }
        public int? CoachId { get; set; }
        public int? CoachPackageId { get; set; }
        public virtual ICollection<DietPlan> DietPlans { get; set; }
        public virtual ICollection<TrainingPlan> TrainingPlans { get; set; }
        public int? PhysiotherapistId { get; set; }
        public int? PhysiotherapistPackageId { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public int? DoctorId { get; set; }
        public int? DoctorPackageId { get; set; }


    }
}