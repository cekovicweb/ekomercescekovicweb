using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class Page
    {
        [Key]
        public int Id  { get;  set; }
        public string URLPages { get; set; }
        public string Title { get; set; } 
        public string Description { get; set; }
        public string Author { get; set; }
        public bool IsPrivate { get; set; }
    }
}