using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class Training
    {
        [Key]
        public int Id { get; set; }
        public string TrainingName { get; set; }
        public string Calorific { get; set; }
        public string Duration { get; set; }
        public int? TrainingPlanId { get; set; }
        public virtual ICollection<TrainingExercise> TrainingExercises { get; set; }
    }
}