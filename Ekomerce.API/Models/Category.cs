using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string NameCategory { get; set; }
        // public ICollection<Product> Product { get; set; }
        // public int ProductId { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
    }
}