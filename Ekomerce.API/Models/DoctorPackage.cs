using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class DoctorPackage
    {
        [Key]
        public int Id { get; set; }
        public string PackageName { get; set; }
        public string Price { get; set; }
        public int? DoctorId { get; set; }
        public virtual ICollection<DoctorEx> DoctorExs { get; set; }
    }
}