using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class Service
    {
        [Key]
        public int Id { get; set; }
        public string ServiceName { get; set; }
        public string Price { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
    }
}