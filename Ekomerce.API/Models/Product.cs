using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public bool IsMain { get; set; }
        public int Cart { get; set; }
        // public string Category { get; set; }
        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }
    }
}