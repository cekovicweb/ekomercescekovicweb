using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class Account
    {
        [Key]
        public int Id { get; set; }
        public string AccountName { get; set; }
        public string IconURL { get; set; }
        public string ImageURL { get; set; }
    }
}