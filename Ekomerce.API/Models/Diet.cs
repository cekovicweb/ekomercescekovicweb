using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class Diet
    {
        [Key]
        public int Id { get; set; }
        public string DietName { get; set; }
        public string Calorific { get; set; }
        public int? DietPlanId { get; set; }
        public virtual ICollection<DietProduct> DietProducts { get; set; }
    }
}