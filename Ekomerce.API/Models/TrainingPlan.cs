using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class TrainingPlan
    {
        [Key]
        public int Id { get; set; }
        public string Time { get; set; }
        public int? UserId { get; set; }
        public int? CoachId { get; set; }
        public virtual ICollection<Training> Trainings { get; set; }
    }
}