using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public string Content { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public int? ArticleId { get; set; }
        public virtual Article Article { get; set; }
    }
}