using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class Article
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public bool ShowTime { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public int? ImageId { get; set; }
        public virtual Image Image { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public double AvgRate { get; set; }
        public virtual ICollection<Rate> Rates { get; set; }
    }
}