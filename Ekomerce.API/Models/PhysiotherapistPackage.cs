using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class PhysiotherapistPackage
    {
        [Key]
        public int Id { get; set; }
        public string PackageName { get; set; }
        public string Price { get; set; }
        public int? PhysiotherapistId { get; set; }
        public virtual ICollection<PhysiotherapistEx> PhysiotherapistsExs { get; set; }
    }
}