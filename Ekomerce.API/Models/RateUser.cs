using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class RateUser
    {
        [Key]
        public int Id { get; set; }
        public double RateValue { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public int? RateUserId { get; set; }
    }
}