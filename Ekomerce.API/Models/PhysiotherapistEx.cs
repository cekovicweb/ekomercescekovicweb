using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class PhysiotherapistEx
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public int? PhysiotherapistPackageId { get; set; }
        // public virtual Diet Diet { get; set; }
    }
}