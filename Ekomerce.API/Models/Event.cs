using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class Event
    {
        [Key]
        public int id { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string title { get; set; }
        public string primary { get; set; }
        public string secondary { get; set; }
        public int? UserId { get; set; }
    }
}