using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class DietProduct
    {
        [Key]
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string Weight { get; set; }
        public string Price { get; set; }
        public string PriceForWeight { get; set; }
        public int? DietId { get; set; }
        // public virtual Diet Diet { get; set; }
    }
}