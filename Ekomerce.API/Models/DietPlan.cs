using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Ekomerce.API.Models
{
    public class DietPlan
    {
        [Key]
        public int Id { get; set; }
        public string Time { get; set; }
        public int? UserId { get; set; }
        public int? CoachId { get; set; }
        public virtual ICollection<Diet> Diets { get; set; }
    }
}