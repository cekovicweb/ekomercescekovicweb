using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class TrainingExercise
    {
        [Key]
        public int Id { get; set; }
        public string ExerciseName { get; set; }
        public string Duration { get; set; }
        public int Reapeat { get; set; }
        public int? TrainingId { get; set; }
         public int? ImageId { get; set; }
        public virtual Image Image { get; set; }

    }
}