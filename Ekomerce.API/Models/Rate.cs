using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class Rate
    {
        [Key]
        public int Id { get; set; }
        public double RateValue { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public int? ArticleId { get; set; }
        public virtual Article Article { get; set; }
    }
}