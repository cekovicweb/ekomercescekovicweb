using System.ComponentModel.DataAnnotations;

namespace Ekomerce.API.Models
{
    public class Image
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public virtual Product Product { get; set; }
        public virtual Article Article { get; set; }
        public virtual User Users { get; set; }
        public virtual TrainingExercise TrainingExercise { get; set; }
    }
}