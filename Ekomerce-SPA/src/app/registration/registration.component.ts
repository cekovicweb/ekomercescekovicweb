import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Account } from '../_modles/account';
import { User } from '../_modles/user';
import { AuthService } from '../_services/-auth.service';
import { AccountService } from '../_services/account.service';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass'],
})
export class RegistrationComponent implements OnInit {
  @Output() cancelRegister = new EventEmitter();
  model: any = {};
  public accountType: string;
  public account: Account;

  constructor(
    private authService: AuthService,
    private alertify: AlertifyService,
    public router: Router,
    private route: ActivatedRoute,
    private accountService: AccountService
  ) {}

  getAccout(id: string): void {
    this.accountService
      .getAccount(id)
      .subscribe((data) => (this.account = data));
  }

  ngOnInit(): void {
    this.accountType = this.route.snapshot.paramMap.get('id');
    this.getAccout(this.accountType);
  }

  register(
    user: string,
    firstName: string,
    lastName: string,
    password: string,
    gender: string,
    dateOfBirth: string,
    zodiacSign: string,
    city: string,
    country: string,
    account: string
  ): void {
    this.authService
      .register(
        (this.model = {
          Username: user,
          Name: firstName,
          LastName: lastName,
          Password: password,
          Gender: gender,
          DateOfBirth: dateOfBirth,
          ZodiacSign: zodiacSign,
          City: city,
          Country: country,
          Created: new Date(),
          AccountType: account,
        })
      )
      .subscribe(
        () => {
          this.router.navigate(['login']);
          this.alertify.success('Rejestracja udana, możesz się zalogować');
        },
        (error) => {
          this.alertify.error(error.error);
        }
      );
  }

  cancel(): void {
    this.cancelRegister.emit(false);
  }
}
