import { Component, OnInit } from '@angular/core';
import { Account } from 'src/app/_modles/account';
import { AccountService } from 'src/app/_services/account.service';

@Component({
  selector: 'app-account-type',
  templateUrl: './account-type.component.html',
  styleUrls: ['./account-type.component.sass'],
})
export class AccountTypeComponent implements OnInit {
  public accounts: Account[];


  constructor(private accountService: AccountService) {}

  ngOnInit(): void {
    this.getAccounts();
  }


  getAccounts(): void {
    this.accountService
      .getAccounts()
      .subscribe((data) => (this.accounts = data));
  }
}
