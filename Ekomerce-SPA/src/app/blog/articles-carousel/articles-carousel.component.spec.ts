import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesCarouselComponent } from './articles-carousel.component';

describe('ArticlesCarouselComponent', () => {
  let component: ArticlesCarouselComponent;
  let fixture: ComponentFixture<ArticlesCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
