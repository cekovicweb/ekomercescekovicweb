import { Component, Input, OnInit } from '@angular/core';
import { RateUser } from 'src/app/_modles/rateUser';
import { User } from 'src/app/_modles/user';
import { AuthService } from 'src/app/_services/-auth.service';
import { Article } from '../../_modles/article';
import { ArticleService } from '../../_services/article.service';

@Component({
  selector: 'app-articles-carousel',
  templateUrl: './articles-carousel.component.html',
  styleUrls: ['./articles-carousel.component.sass'],
})
export class ArticlesCarouselComponent implements OnInit {
  @Input() public getArticle = false;
  @Input() public getUser = false;
  articles: Article[];
  users: User[];
  bestUserIndex: Array<number> = [];
  public responsiveOptions: any;
  public rateUser: number;

  constructor(
    private articleService: ArticleService,
    private authService: AuthService
  ) {
    this.responsiveOptions = [
      {
        breakpoint: '2024px',
        numVisible: 3,
        numScroll: 1,
      },
      {
        breakpoint: '1700px',
        numVisible: 2,
        numScroll: 1,
      },
      {
        breakpoint: '1300px',
        numVisible: 1,
        numScroll: 1,
      },
    ];
  }

  ngOnInit(): void {
    if (this.getArticle) {
      this.getArticles();
    }

    if (this.getUser) {
      this.getUsers();
    }
  }

  getArticles(): void {
    this.articleService.getArticles().subscribe((data) => {
      this.articles = data;

      data.forEach((a) => {
        if (a.Comments.length >= 10) {
          if (this.avgUserRate(a.User.RateUsers) > 3) {
            if (!this.bestUserIndex.includes(a.UserId)) {
              this.bestUserIndex.push(a.UserId);
            }
          }
        }
      });
      if (this.users) {
        this.users = this.users.filter(
          (u) =>
            u.AccountType !== 'użytkownik' && this.bestUserIndex.includes(u.Id)
        );
      }
    });
  }

  getUsers(): void {
    this.authService.getUsers().subscribe((data) => {
      this.users = data;
      this.getArticles();
    });
  }

  avgUserRate(rates: Array<RateUser>): number {
    let suma = 0;
    let i = 0;
    for (const rate of rates) {
      ++i;
      suma += rate.RateValue;
    }
    this.rateUser = suma / i;

    return suma / i;
  }
}
