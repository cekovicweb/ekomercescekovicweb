import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Article } from '../_modles/article';
import { Comment } from '../_modles/comment';
import { Rate } from '../_modles/rate';
import { RateUser } from '../_modles/rateUser';
import { AuthService } from '../_services/-auth.service';
import { ArticleService } from '../_services/article.service';
import { CommentService } from '../_services/comment.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.sass'],
})
export class BlogComponent implements OnInit {
  articles: Observable<Article[]>;
  comments = [];
  public token: any;
  public tokenId: any;
  public tokenIdInt: number;
  public index: number;
  public ratePost: number;
  public rateUser: number;
  public val2: any;

  form = new FormGroup({
    id: new FormControl(''),
    content: new FormControl('', Validators.required),
  });

  constructor(
    private articleService: ArticleService,
    private commentService: CommentService,
    private authService: AuthService
  ) {}

  logged(): boolean {
    return this.authService.loggedIn();
  }

  showHideComments(id: number): void {
    if (this.index === id) {
      this.index = null;
    } else {
      this.index = id;
    }
  }

  ngOnInit(): void {
    this.getArticles();
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
    }
  }

  avgRate(rates: Array<Rate>): void {
    let suma = 0;
    let i = 0;
    for (const rate of rates) {
      ++i;
      suma += rate.RateValue;
    }
    this.ratePost = suma / i;
  }

  avgUserRate(rates: Array<RateUser>): void {
    let suma = 0;
    let i = 0;
    for (const rate of rates) {
      ++i;
      suma += rate.RateValue;
    }
    this.rateUser = suma / i;
  }

  getArticles(): void {
    // this.articleService.getArticles().subscribe((data) => {
    //   this.articles = data;
    //   this.sortPosts(this.articles, 'new');
    // });
    this.articles = this.articleService.getArticles();
    this.sortPosts('new');
  }

  addComment(articleId: number): void {
    this.commentService
      .addComment({
        Content: this.form.value.content.toString(),
        UserId: this.tokenIdInt,
        ArticleId: articleId,
      } as Comment)
      .subscribe((comment) => {
        this.comments.push(comment);
        this.getArticles();
        this.form.reset();
      });
  }

  deleteComment(comment: Comment): void {
    this.commentService
      .deleteComment(comment)
      .subscribe(() => this.getArticles());
  }

  sortPosts(sort: string): any {
    if (sort === 'new') {
      // return arr.sort((a, b) => b.Id - a.Id);
      this.articles = this.articles.pipe(
        map((data) => {
          data.sort((a, b) => {
            return b.Id - a.Id;
          });
          return data;
        })
      );
    }
    if (sort === 'old') {
      // return arr.sort((a, b) => a.Id - b.Id);
      this.articles = this.articles.pipe(
        map((data) => {
          data.sort((a, b) => {
            return a.Id - b.Id;
          });
          return data;
        })
      );
    }
  }
}
