import { Image } from './image';
import { User } from './user';
import { Comment } from './comment';
import { Rate } from './rate';

export interface Article {
    Id: number;
    Title: string;
    ShortDesc: string;
    LongDesc: string;
    Date: string;
    Time: string;
    ShowTime: boolean;
    UserId: number;
    User: User;
    ImageId: number;
    Image: Image;
    Comments: Array<Comment>;
    Rates: Array<Rate>;
  }
