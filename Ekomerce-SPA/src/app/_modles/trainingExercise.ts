import { Image } from './image';

export interface TrainingExercise{
    Id: number;
    ExerciseName: string;
    Duration: string;
    Reapeat: number;
    TrainingId: number;
    ImageId: number;
    Image: Image;
}
