import { Diet } from './diet';

export interface DietPlan {
    Id: number;
    Time: string;
    UserId: number;
    CoachId: number;
    Diets: Array<Diet>;
  }
