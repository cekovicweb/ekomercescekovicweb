export interface Service {
    Id: number;
    ServiceName: string;
    Price: string;
    UserId: number;
  }
