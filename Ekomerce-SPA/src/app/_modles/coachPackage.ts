import { User } from './user';

export interface CoachPackage {
    Id: number;
    PackageName: string;
    Price: string;
    UserId: number;
    CoachId: number;
    Users: Array<User>;
  }
