import { RateUser } from './rateUser';
import { Image } from './image';
import { DietPlan } from './dietPlan';
import { TrainingPlan } from './trainingPlan';

export interface User {
  Id: number;
  Name: string;
  LastName: string;
  Username: string;
  Gender: string;
  DateOfBirth: any;
  ZodiacSign: string;
  Created: any;
  LastActive: any;
  City: string;
  Country: string;
  AccountType: string;
  RateUsers: Array<RateUser>;
  ImageId: number;
  Image: Image;
  CoachId: number;
  CoachPackageId: number;
  DietPlans: Array<DietPlan>;
  TrainingPlans: Array<TrainingPlan>;
  PhysiotherapistId: number;
  PhysiotherapistPackageId: number;
  DoctorId: number;
  DoctorPackageId: number;
}
