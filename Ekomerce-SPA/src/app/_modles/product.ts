import { Category } from './category';
import { User } from './user';
import { Image } from './image';

export interface Product {
  Id: number;
  Name: string;
  Description: string;
  Price: string;
  IsMain: boolean;
  Cart: number;
  CategoryId: number;
  Category: Category;
  UserId: number;
  User: User;
  ImageId: number;
  Image: Image;
}
