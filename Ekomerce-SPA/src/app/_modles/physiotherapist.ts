import { PhysiotherapistEx } from './physiotherapistEx';
import { User } from './user';

export interface PhysiotherapistPackage {
    Id: number;
    PackageName: string;
    Price: string;
    UserId: number;
    PhysiotherapistId: number;
    Users: Array<User>;
    PhysiotherapistsExs: Array<PhysiotherapistEx>;
  }
