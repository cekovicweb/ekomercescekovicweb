import { Training } from './training';

export interface TrainingPlan {
    Id: number;
    Time: string;
    UserId: number;
    CoachId: number;
    Trainings: Array<Training>;
}
