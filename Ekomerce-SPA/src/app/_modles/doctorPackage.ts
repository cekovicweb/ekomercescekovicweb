import { DoctorEx } from './doctorEx';
import { User } from './user';

export interface DoctorPackage {
    Id: number;
    PackageName: string;
    Price: string;
    UserId: number;
    DoctorId: number;
    Users: Array<User>;
    DoctorExs: Array<DoctorEx>;
  }
