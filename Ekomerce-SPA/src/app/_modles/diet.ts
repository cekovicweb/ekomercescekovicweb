import { DietProduct } from './dietProduct';

export interface Diet {
    Id: number;
    DietName: string;
    Calorific: string;
    DietPlanId: number;
    DietProducts: Array<DietProduct>;
}
