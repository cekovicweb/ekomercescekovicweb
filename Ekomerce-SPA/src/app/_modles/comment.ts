import { User } from './user';

export interface Comment {
    Id: number;
    Content: string;
    UserId: number;
    User: User;
    ArticleId: number;
  }
