export interface Account {
    Id: number;
    AccountName: string;
    IconURL: string;
    ImageURL: string;
}
