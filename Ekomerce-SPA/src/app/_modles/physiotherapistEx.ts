export interface PhysiotherapistEx {
    Id: number;
    Name: string;
    Price: string;
    PhysiotherapistPackageId: number;
}
