export interface DietProduct {
    Id: number;
    ProductName: string;
    Weight: string;
    Price: string;
    PriceForWeight: string;
    DietId: number;
}
