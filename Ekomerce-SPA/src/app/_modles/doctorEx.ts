export interface DoctorEx {
    Id: number;
    Name: string;
    Price: string;
    DoctorPackageId: number;
}
