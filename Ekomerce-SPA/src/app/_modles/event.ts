import { CalendarEvent } from 'angular-calendar';

export interface Event extends CalendarEvent {
  UserId?: number;
  primary: string;
  secondary: string;
}
