import { TrainingExercise } from './trainingExercise';

export interface Training{
    Id: number;
    TrainingName: string;
    Calorific: string;
    Duration: string;
    TrainingPlanId: number;
    TrainingExercises: Array<TrainingExercise>;
}
