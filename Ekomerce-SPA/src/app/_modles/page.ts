export interface Page {
  id: number;
  uRLPages: string;
  title: string;
  description: string;
  author: string;
  isPrivate: boolean;
}
