export interface Image {
    Id: number;
    Name: string;
    Path: string;
}
