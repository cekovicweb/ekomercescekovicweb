import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Category } from '../_modles/category';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getCategorys(): Observable<Category[]> {
    return this.http.get<Category[]>(this.baseUrl + 'category');
  }

  getCategory(id: string): Observable<Category> {
    return this.http.get<Category>(this.baseUrl + 'category/' + id);
  }
}
