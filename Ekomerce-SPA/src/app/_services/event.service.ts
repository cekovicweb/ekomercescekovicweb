import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Event } from '../_modles/event';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class EventService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getEvent(id: number): Observable<Event> {
    return this.http.get<Event>(this.baseUrl + 'event/' + id);
  }

  getEvents(): Observable<Event[]>{
    return this.http.get<Event[]>(this.baseUrl + 'event');
  }

  addEvent(event: Event): Observable<Event> {
    return this.http.post<Event>(
      this.baseUrl + 'event',
      JSON.stringify(event),
      httpOptions
    );
  }

  updateEvent(event: Event): Observable<any> {
    return this.http.put(
      this.baseUrl + 'event/' + event.id,
      JSON.stringify(event),
      httpOptions
    );
  }

  deleteEvent(event: Event): Observable<Event> {
    const id = typeof event === 'number' ? event : event.id;
    return this.http.delete<Event>(
      this.baseUrl + 'event/' + id,
      httpOptions
    );
  }
}
