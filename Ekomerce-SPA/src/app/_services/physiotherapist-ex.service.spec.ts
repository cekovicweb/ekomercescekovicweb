import { TestBed } from '@angular/core/testing';

import { PhysiotherapistExService } from './physiotherapist-ex.service';

describe('PhysiotherapistExService', () => {
  let service: PhysiotherapistExService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhysiotherapistExService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
