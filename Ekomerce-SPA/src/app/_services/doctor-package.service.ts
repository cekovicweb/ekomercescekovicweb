import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DoctorPackage } from '../_modles/doctorPackage';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class DoctorPackageService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getPackages(): Observable<DoctorPackage[]> {
    return this.http.get<DoctorPackage[]>(this.baseUrl + 'doctorpackage');
  }

  getPackage(id: number): Observable<DoctorPackage> {
    return this.http.get<DoctorPackage>(this.baseUrl + 'doctorpackage/' + id);
  }

  addPackage(doctorPackage: DoctorPackage): Observable<DoctorPackage> {
    return this.http.post<DoctorPackage>(
      this.baseUrl + 'doctorpackage',
      JSON.stringify(doctorPackage),
      httpOptions
    );
  }

  updatePackage(doctorPackage: DoctorPackage): Observable<any> {
    return this.http.put(
      this.baseUrl + 'doctorpackage/' + doctorPackage.Id,
      JSON.stringify(doctorPackage),
      httpOptions
    );
  }
}
