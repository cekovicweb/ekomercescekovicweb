import { TestBed } from '@angular/core/testing';

import { DoctorExService } from './doctor-ex.service';

describe('DoctorExService', () => {
  let service: DoctorExService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DoctorExService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
