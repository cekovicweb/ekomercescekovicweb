import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Comment } from '../_modles/comment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class CommentService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getComments(): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.baseUrl + 'comment');
  }

  getComment(id: string): Observable<Comment> {
    return this.http.get<Comment>(this.baseUrl + 'comment/' + id);
  }

  addComment(comment: Comment): Observable<Comment> {
    return this.http.post<Comment>(
      this.baseUrl + 'comment',
      JSON.stringify(comment),
      httpOptions
    );
  }

  deleteComment(comment: Comment): Observable<Comment> {
    const id = typeof comment === 'number' ? comment : comment.Id;
    return this.http.delete<Comment>(
      this.baseUrl + 'comment/' + id,
      httpOptions
    );
  }

  updateComment(comment: Comment): Observable<any> {
    return this.http.put(
      this.baseUrl + 'comment/' + comment.Id,
      JSON.stringify(comment),
      httpOptions
    );
  }
}
