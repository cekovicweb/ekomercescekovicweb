import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DoctorEx } from '../_modles/doctorEx';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class DoctorExService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getMedicalExs(): Observable<DoctorEx[]> {
    return this.http.get<DoctorEx[]>(this.baseUrl + 'doctorex');
  }

  getMedicalEx(id: number): Observable<DoctorEx> {
    return this.http.get<DoctorEx>(this.baseUrl + 'doctorex/' + id);
  }

  addMedicalEx(doctorEx: DoctorEx): Observable<DoctorEx> {
    return this.http.post<DoctorEx>(
      this.baseUrl + 'doctorex',
      JSON.stringify(doctorEx),
      httpOptions
    );
  }

  updateMedicalEx(doctorEx: DoctorEx): Observable<any> {
    return this.http.put(
      this.baseUrl + 'doctorex/' + doctorEx.Id,
      JSON.stringify(doctorEx),
      httpOptions
    );
  }

  deleteMedicineEx(doctorEx: DoctorEx): Observable<DoctorEx> {
    const id = typeof doctorEx === 'number' ? doctorEx : doctorEx.Id;
    return this.http.delete<DoctorEx>(
      this.baseUrl + 'doctorex/' + id,
      httpOptions
    );
  }
}
