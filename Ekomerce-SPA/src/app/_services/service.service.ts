import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Service } from '../_modles/service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class ServiceService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getServices(): Observable<Service[]> {
    return this.http.get<Service[]>(this.baseUrl + 'service');
  }

  getService(id: string): Observable<Service> {
    return this.http.get<Service>(this.baseUrl + 'service/' + id);
  }

  addService(service: Service): Observable<Service> {
    return this.http.post<Service>(
      this.baseUrl + 'service',
      JSON.stringify(service),
      httpOptions
    );
  }

  deleteService(service: Service): Observable<Service> {
    const id = typeof service === 'number' ? service : service.Id;
    return this.http.delete<Service>(
      this.baseUrl + 'service/' + id,
      httpOptions
    );
  }

  updateService(service: Service): Observable<any> {
    return this.http.put(
      this.baseUrl + 'service/' + service.Id,
      JSON.stringify(service),
      httpOptions
    );
  }
}
