import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './-auth.service';
import { AlertifyService } from './alertify.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(
    private authService: AuthService,
    public router: Router,
    private alertify: AlertifyService
  ) {}

  canActivate(): boolean {
    if (!this.authService.loggedIn()) {
      this.router.navigate(['login']);
      this.alertify.error('Musisz się zalogować!')
      return false;
    }
    return true;
  }
}
