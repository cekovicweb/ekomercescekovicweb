import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PhysiotherapistEx } from '../_modles/physiotherapistEx';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class PhysiotherapistExService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getMedicalExs(): Observable<PhysiotherapistEx[]> {
    return this.http.get<PhysiotherapistEx[]>(this.baseUrl + 'physiotherapistex');
  }

  getMedicalEx(id: number): Observable<PhysiotherapistEx> {
    return this.http.get<PhysiotherapistEx>(this.baseUrl + 'physiotherapistex/' + id);
  }

  addMedicalEx(physiotherapistex: PhysiotherapistEx): Observable<PhysiotherapistEx> {
    return this.http.post<PhysiotherapistEx>(
      this.baseUrl + 'physiotherapistex',
      JSON.stringify(physiotherapistex),
      httpOptions
    );
  }

  updateMedicalEx(physiotherapistex: PhysiotherapistEx): Observable<any> {
    return this.http.put(
      this.baseUrl + 'physiotherapistex/' + physiotherapistex.Id,
      JSON.stringify(physiotherapistex),
      httpOptions
    );
  }

  deleteMedicineEx(physiotherapistex: PhysiotherapistEx): Observable<PhysiotherapistEx> {
    const id = typeof physiotherapistex === 'number' ? physiotherapistex : physiotherapistex.Id;
    return this.http.delete<PhysiotherapistEx>(
      this.baseUrl + 'physiotherapistex/' + id,
      httpOptions
    );
  }
}
