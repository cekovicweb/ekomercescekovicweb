import { TestBed } from '@angular/core/testing';

import { DoctorPackageService } from './doctor-package.service';

describe('DoctorPackageService', () => {
  let service: DoctorPackageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DoctorPackageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
