import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Diet } from '../_modles/diet';
import { DietPlan } from '../_modles/dietPlan';
import { DietProduct } from '../_modles/dietProduct';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class DietService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getDietPlans(): Observable<DietPlan[]>{
    return this.http.get<DietPlan[]>(this.baseUrl + 'dietplan');
  }

  addDietPlan(dietPlan: DietPlan): Observable<DietPlan> {
    return this.http.post<DietPlan>(
      this.baseUrl + 'dietplan',
      JSON.stringify(dietPlan),
      httpOptions
    );
  }

  addDiet(diet: Diet): Observable<Diet> {
    return this.http.post<Diet>(
      this.baseUrl + 'diet',
      JSON.stringify(diet),
      httpOptions
    );
  }

  updateDiet(diet: Diet): Observable<any> {
    return this.http.put(
      this.baseUrl + 'diet/' + diet.Id,
      JSON.stringify(diet),
      httpOptions
    );
  }

  addProduct(product: DietProduct): Observable<DietProduct>{
    return this.http.post<DietProduct>(
      this.baseUrl + 'dietproduct',
      JSON.stringify(product),
      httpOptions
    );
  }

  updateProduct(product: DietProduct): Observable<any> {
    return this.http.put(
      this.baseUrl + 'dietproduct/' + product.Id,
      JSON.stringify(product),
      httpOptions
    );
  }
}
