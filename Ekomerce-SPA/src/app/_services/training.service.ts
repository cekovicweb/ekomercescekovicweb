import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Training } from '../_modles/training';
import { TrainingPlan } from '../_modles/trainingPlan';
import { TrainingExercise } from '../_modles/trainingExercise';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class TrainingService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}


  getTrainingPlans(): Observable<TrainingPlan[]>{
    return this.http.get<TrainingPlan[]>(this.baseUrl + 'trainingplan');
  }

  addTrainingPlan(trainingPlan: TrainingPlan): Observable<TrainingPlan> {
    return this.http.post<TrainingPlan>(
      this.baseUrl + 'trainingplan',
      JSON.stringify(trainingPlan),
      httpOptions
    );
  }

  addTraining(training: Training): Observable<Training> {
    return this.http.post<Training>(
      this.baseUrl + 'training',
      JSON.stringify(training),
      httpOptions
    );
  }

  updateTraining(training: Training): Observable<any> {
    return this.http.put(
      this.baseUrl + 'training/' + training.Id,
      JSON.stringify(training),
      httpOptions
    );
  }

  addExercise(exercise: TrainingExercise): Observable<TrainingExercise>{
    return this.http.post<TrainingExercise>(
      this.baseUrl + 'trainingexercise',
      JSON.stringify(exercise),
      httpOptions
    );
  }

  updateExercise(exercise: TrainingExercise): Observable<any> {
    return this.http.put(
      this.baseUrl + 'trainingexercise/' + exercise.Id,
      JSON.stringify(exercise),
      httpOptions
    );
  }
}
