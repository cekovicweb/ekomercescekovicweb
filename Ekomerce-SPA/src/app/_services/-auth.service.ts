import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../_modles/user';
import { AlertifyService } from './alertify.service';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  baseUrl = environment.apiUrl;
  jwtHelper = new JwtHelperService();
  decodedToken: any;

  constructor(
    private http: HttpClient,
    private alertify: AlertifyService,
    public router: Router
  ) {}

  login(model: any): Observable<void> {
    return this.http.post(this.baseUrl + 'auth/login', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token);
          this.decodedToken = this.jwtHelper.decodeToken(user.token);
        }
      })
    );
  }

  register(model: any): Observable<any> {
    return this.http.post(this.baseUrl + 'auth/register', model);
  }

  loggedIn(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  logOut(): void {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
    this.alertify.warning('Zostałeś wylogowany');
  }

  getUser(id: any): Observable<User> {
    return this.http.get<User>(this.baseUrl + 'users/' + id);
  }

  getUsers(): Observable<User[]>{
    return this.http.get<User[]>(this.baseUrl + 'users');
  }


  updateUser(user: User): Observable<any> {
    return this.http.put(this.baseUrl + 'users/' + user.Id, JSON.stringify(user), httpOptions);
  }

  getTokenId(): number{
    const token = localStorage.getItem('token');
    return this.jwtHelper.decodeToken(token);
  }
}
