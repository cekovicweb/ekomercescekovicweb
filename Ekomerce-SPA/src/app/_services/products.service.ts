import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Product } from 'src/app/_modles/product';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  public cart = [];
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {
    if (localStorage.getItem('cart')) {
      this.cart = JSON.parse(localStorage.getItem('cart'));
    }
  }

  addToCart(prod): any {
    const a = this.cart.find((z) => z.Id === prod.Id);
    if (this.cart.includes(a)) {
      const index = this.cart.findIndex((x) => x.Id === prod.Id);
      this.cart[index].Cart++;
    } else {
      this.cart.push(prod);
      const index = this.cart.findIndex((x) => x.Id === prod.Id);
      this.cart[index].Cart++;
    }
    localStorage.setItem('cart', JSON.stringify(this.cart));
  }

  getProduct(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl + 'product');
  }

  getOneProduct(id: string): Observable<Product> {
    return this.http.get<Product>(this.baseUrl + 'product/' + id);
  }

  addProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(
      this.baseUrl + 'product',
      JSON.stringify(product),
      httpOptions
    );
  }

  deleteProduct(product: Product): Observable<Product> {
    const id = typeof product === 'number' ? product : product.Id;
    return this.http.delete<Product>(
      this.baseUrl + 'product/' + id,
      httpOptions
    );
  }

  updateProduct(product: Product): Observable<any> {
    return this.http.put(
      this.baseUrl + 'product/' + product.Id,
      JSON.stringify(product),
      httpOptions
    );
  }

  // AddProduct() {
  //   this.getProduct().subscribe(
  //     (data: Product) => this.product = {
  //       id: data.id,
  //       name: data.name,
  //       description: data.description,
  //       uRLPhoto: data.uRLPhoto,
  //       price: data.price,
  //       isMain: data.isMain
  //      }
  //   );
  // }

  priceToFloat(price): number {
    price.replace(/,/g, '.');
    return price.replace(/,/g, '.');
  }
}
