import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PhysiotherapistPackage } from '../_modles/physiotherapist';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class PhysiotherapistPackageService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getPackages(): Observable<PhysiotherapistPackage[]> {
    return this.http.get<PhysiotherapistPackage[]>(this.baseUrl + 'physiotherapistpackage');
  }

  getPackage(id: number): Observable<PhysiotherapistPackage> {
    return this.http.get<PhysiotherapistPackage>(this.baseUrl + 'physiotherapistpackage/' + id);
  }

  addPackage(physiotherapistPackage: PhysiotherapistPackage): Observable<PhysiotherapistPackage> {
    return this.http.post<PhysiotherapistPackage>(
      this.baseUrl + 'physiotherapistpackage',
      JSON.stringify(physiotherapistPackage),
      httpOptions
    );
  }

  updatePackage(physiotherapistPackage: PhysiotherapistPackage): Observable<any> {
    return this.http.put(
      this.baseUrl + 'physiotherapistpackage/' + physiotherapistPackage.Id,
      JSON.stringify(physiotherapistPackage),
      httpOptions
    );
  }
}
