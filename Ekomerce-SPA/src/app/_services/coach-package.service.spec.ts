import { TestBed } from '@angular/core/testing';

import { CoachPackageService } from './coach-package.service';

describe('CoachPackageService', () => {
  let service: CoachPackageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoachPackageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
