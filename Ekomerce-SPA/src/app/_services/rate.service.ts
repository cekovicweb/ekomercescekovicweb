import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Rate } from '../_modles/rate';
import { RateUser } from '../_modles/rateUser';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class RateService {

  baseUrl =  environment.apiUrl;

  constructor(private http: HttpClient) { }

  // getServices(): Observable<Service[]> {
  //   return this.http.get<Service[]>(this.baseUrl + 'service')
  // }

  // getService(id: string): Observable<Service> {
  //   return this.http.get<Service>(this.baseUrl + 'service/' + id)
  // }

  addRate(rate: Rate): Observable<Rate> {
    return this.http.post<Rate>(this.baseUrl + 'rate', JSON.stringify(rate), httpOptions);
  }

  addUserRate(rateUser: RateUser): Observable<RateUser>{
    return this.http.post<RateUser>(this.baseUrl + 'rateuser', JSON.stringify(rateUser), httpOptions);
  }

}
