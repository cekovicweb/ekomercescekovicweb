import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Article } from '../_modles/article';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.baseUrl + 'article');
  }

  getArticle(id: string): Observable<Article> {
    return this.http.get<Article>(this.baseUrl + 'article/' + id);
  }

  addArticle(article: Article): Observable<Article> {
    return this.http.post<Article>(
      this.baseUrl + 'article',
      JSON.stringify(article),
      httpOptions
    );
  }

  deleteArticle(article: Article): Observable<Article> {
    const id = typeof article === 'number' ? article : article.Id;
    return this.http.delete<Article>(
      this.baseUrl + 'article/' + id,
      httpOptions
    );
  }

  updateArticle(article: Article): Observable<any> {
    return this.http.put(
      this.baseUrl + 'article/' + article.Id,
      JSON.stringify(article),
      httpOptions
    );
  }
}
