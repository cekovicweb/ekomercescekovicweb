import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CoachPackage } from '../_modles/coachPackage';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root'
})
export class CoachPackageService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) {}

  getPackages(): Observable<CoachPackage[]> {
    return this.http.get<CoachPackage[]>(this.baseUrl + 'coachpackage');
  }

  getPackage(id: number): Observable<CoachPackage> {
    return this.http.get<CoachPackage>(this.baseUrl + 'coachpackage/' + id);
  }

  addPackage(coachPackage: CoachPackage): Observable<CoachPackage> {
    return this.http.post<CoachPackage>(
      this.baseUrl + 'coachpackage',
      JSON.stringify(coachPackage),
      httpOptions
    );
  }

  updatePackage(coachPackage: CoachPackage): Observable<any> {
    return this.http.put(
      this.baseUrl + 'coachpackage/' + coachPackage.Id,
      JSON.stringify(coachPackage),
      httpOptions
    );
  }
}
