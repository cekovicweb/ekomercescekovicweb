import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Image } from '../_modles/image';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  baseUrl =  environment.apiUrl;

  constructor(private http: HttpClient) { }

  addImage(image: FormData): Observable<Image>{
    return this.http.post<Image>(this.baseUrl + 'image', image);
  }

  deleteImage(image: Image): Observable<Image>{
    const id = typeof image === 'number' ? image : image.Id;
    return this.http.delete<Image>(this.baseUrl + 'image/' + id);
  }

  updateImage(newImage: FormData, oldImage: Image): void{
    this.deleteImage(oldImage);
    this.addImage(newImage);
  }
}
