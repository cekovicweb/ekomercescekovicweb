import { Component, OnInit } from '@angular/core';
import { User } from '../_modles/user';
import { AuthService } from '../_services/-auth.service';
import { AccountService } from '../_services/account.service';
import { RateUser } from '../_modles/rateUser';
import { RateService } from '../_services/rate.service';
import { AlertifyService } from '../_services/alertify.service';
import { CoachPackage } from '../_modles/coachPackage';
import { CoachPackageService } from '../_services/coach-package.service';
import { PhysiotherapistPackage } from '../_modles/physiotherapist';
import { PhysiotherapistPackageService } from '../_services/physiotherapist-package.service';
import { DoctorPackage } from '../_modles/doctorPackage';
import { DoctorPackageService } from '../_services/doctor-package.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.sass'],
})
export class UsersListComponent implements OnInit {
  public token: any;
  public tokenId: any;
  public tokenIdInt: number;
  public loggedUser: User;
  public users: User[] = [];
  public accounts = [];
  public filterUsers = [];
  public rateUser: number;
  public packages: CoachPackage[];
  public coachId: number;
  public physioPackages: PhysiotherapistPackage[];
  public physiotherapistId: number;
  public doctorPackages: DoctorPackage[];
  public doctortId: number;
  public calendar = false;
  public userID: number;
  public account: string;

  constructor(
    private authService: AuthService,
    private rateService: RateService,
    private alertifyService: AlertifyService,
    private coachPackageService: CoachPackageService,
    private physiotherapistPackageService: PhysiotherapistPackageService,
    private doctorPackageService: DoctorPackageService
  ) {}

  showCalendar(id: number): void {
    if (this.userID !== id){
      this.userID = id;
      this.calendar = true;
    } else {
      this.calendar = !this.calendar;
      this.userID = 0;
    }
  }

  getUsers(): void {
    // this.users = [];
    this.accounts = [];
    this.authService.getUsers().subscribe((data) => {
      this.users = data;
      for (const user of data) {
        if (
          !this.accounts.includes(user.AccountType) &&
          user.AccountType !== 'użytkownik'
        ) {
          this.accounts.push(user.AccountType);
        }
      }
      this.filterUser(this.users, 'Wszystkie');
    });
  }

  getLoggedUser(): void {
    this.authService.getUser(this.tokenIdInt).subscribe((data) => {
      this.loggedUser = data;
      this.coachId = data.CoachId;
      this.account = data.AccountType;
    });
  }

  getPackages(): void {
    this.coachPackageService
      .getPackages()
      .subscribe((data) => (this.packages = data));
  }
  getPhysioPackages(): void {
    this.physiotherapistPackageService
      .getPackages()
      .subscribe((data) => (this.physioPackages = data));
  }
  getDoctorPackages(): void {
    this.doctorPackageService
      .getPackages()
      .subscribe((data) => (this.doctorPackages = data));
  }

  filterUser(arr: User[], type: string): User[] {
    // if (type === 'Wszystkie') {
    //   this.filterUsers = [];
    //   this.users.forEach((u) => {
    //     if (u.AccountType !== 'użytkownik') {
    //       this.filterUsers.push(Object.assign({}, u));
    //     }
    //   });
    // } else {
    //   this.filterUsers = this.users.filter((user) => user.AccountType === type);
    // }
    if (type === 'Wszystkie') {
      this.filterUsers = arr.filter(
        (user) => user.AccountType !== 'użytkownik'
      );
      return this.filterUsers;
    } else {
      this.filterUsers = arr.filter((user) => user.AccountType === type);
      return this.filterUsers;
    }
    // this.getLoggedUser();
  }

  avgUserRate(rates: Array<RateUser>): void {
    let suma = 0;
    let i = 0;
    for (const rate of rates) {
      ++i;
      suma += rate.RateValue;
    }
    this.rateUser = suma / i;
  }

  canRateUser(user: User): boolean {
    if (this.tokenIdInt === user.Id) {
      return false;
    }
    for (const ra of user.RateUsers) {
      if (this.tokenIdInt === ra.RateUserId) {
        // this.rateUser = ra.RateValue;
        return false;
      }
    }
    return true;
  }

  addUserRate(rateUser: number, user: User): void {
    this.rateService
      .addUserRate({
        RateValue: rateUser,
        UserId: user.Id,
        RateUserId: this.tokenIdInt,
      } as RateUser)
      .subscribe(() => {
        this.canRateUser(user);
        this.getUsers();
      });
  }

  logged(): boolean {
    return this.authService.loggedIn();
  }

  ngOnInit(): void {
    this.getUsers();
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
      this.getLoggedUser();
    }
    this.getPackages();
    this.getPhysioPackages();
    this.getDoctorPackages();
  }

  chooseCoach(
    coachphysId: number,
    userId: number,
    packageId: number,
    accountType: string
  ): void {
    if (accountType === 'trener') {
      this.authService
        .updateUser({
          Id: userId,
          ImageId: this.loggedUser.ImageId,
          CoachId: coachphysId,
          CoachPackageId: packageId,
          PhysiotherapistId: this.loggedUser.PhysiotherapistId,
          PhysiotherapistPackageId: this.loggedUser.PhysiotherapistPackageId,
          DoctorId: this.loggedUser.DoctorId,
          DoctorPackageId: this.loggedUser.PhysiotherapistPackageId,
        } as User)
        .subscribe(() => {
          this.getLoggedUser();
          this.alertifyService.success('Wybrałeś trenera!');
        });
    } else if (accountType === 'Fizjoterapeuta') {
      this.authService
        .updateUser({
          Id: userId,
          ImageId: this.loggedUser.ImageId,
          CoachId: this.loggedUser.CoachId,
          CoachPackageId: this.loggedUser.CoachPackageId,
          PhysiotherapistId: coachphysId,
          PhysiotherapistPackageId: packageId,
          DoctorId: this.loggedUser.DoctorId,
          DoctorPackageId: this.loggedUser.PhysiotherapistPackageId,
        } as User)
        .subscribe(() => {
          this.getLoggedUser();
          this.alertifyService.success('Wybrałeś fizjoterapeutę!');
        });
    } else if (accountType === 'Lekarz sportowy') {
      this.authService
        .updateUser({
          Id: userId,
          ImageId: this.loggedUser.ImageId,
          CoachId: this.loggedUser.CoachId,
          CoachPackageId: this.loggedUser.CoachPackageId,
          PhysiotherapistId: this.loggedUser.PhysiotherapistId,
          PhysiotherapistPackageId: this.loggedUser.PhysiotherapistPackageId,
          DoctorId: coachphysId,
          DoctorPackageId: packageId,
        } as User)
        .subscribe(() => {
          this.getLoggedUser();
          this.alertifyService.success('Wybrałeś lekarza sportowego!');
        });
    }
  }
}
