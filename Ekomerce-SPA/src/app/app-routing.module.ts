import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { PageComponent } from './page/page.component';
import { UserComponent } from './user/user.component';
import { LandingComponent } from './landing/landing.component';
import { ContactComponent } from './contact/contact.component';
import { ShopComponent } from './shop/shop.component';
import { ShopProductsComponent } from './shop/shop-products/shop-products.component';
import { CartComponent } from './shop/cart/cart.component';
import { DeliveryComponent } from './shop/delivery/delivery.component';
import { HelpComponent } from './help/help.component';
import { CategoryComponent } from './shop/category/category.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { ProductDetailsComponent } from './shop/product-details/product-details.component';
import {AuthGuardService as AuthGuard} from './_services/auth-guard.service';
import { AccountTypeComponent } from './registration/account-type/account-type.component';
import { ArticleDetailsComponent } from './user/articles/article-details/article-details.component';
import { BlogComponent } from './blog/blog.component';
import { UsersListComponent } from './users-list/users-list.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { YourUsersDetailsComponent } from './user/dashboard/your-users/your-users-details/your-users-details.component';

const routes: Routes = [
  { path: 'page', component: PageComponent },
  { path: 'nav', component: NavComponent },
  { path: '', component: LandingComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'user', component: UserComponent, canActivate: [AuthGuard] },
  { path: 'user/dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'user/dashboard/users/:id', component: YourUsersDetailsComponent, canActivate: [AuthGuard] },
  { path: 'shop', component: ShopComponent },
  {
    path: 'shop', runGuardsAndResolvers: 'always', children: [
      { path: 'products', component: ShopProductsComponent },
      { path: 'products/:ID', component: ProductDetailsComponent },
      { path: 'cart', component: CartComponent },
      { path: 'delivery', component: DeliveryComponent },
      { path: 'category/:id', component: CategoryComponent }
    ]
  },
  { path: 'registration', component: AccountTypeComponent },
  { path: 'registration/:id', component: RegistrationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'help', component: HelpComponent },
  { path: 'article/:id', component: ArticleDetailsComponent },
  { path: 'blog', component: BlogComponent},
  { path: 'users', component: UsersListComponent},
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
