import { Component, OnInit } from '@angular/core';
// import { parse } from 'path';
import { Product } from 'src/app/_modles/product';
import { ProductsService } from 'src/app/_services/products.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  public allCart = [];
  public products: Product[];
  public ammount: number;
  constructor(private productService: ProductsService) {
    this.updateCartItem();
  }

  updateCart(): any[] {
    return (this.allCart = JSON.parse(localStorage.getItem('cart')));
  }
  updateAmmount(): number {
    if (this.allCart) {
      this.ammount = 0.0;
      for (const prod of this.allCart) {
        this.ammount += prod.Cart * this.priceToFloat(prod.Price);
      }

      return (this.ammount = parseFloat(this.ammount.toFixed(2)));
    }
  }
  updateCount(prod, n): void {
    if (this.allCart.includes(prod)) {
      const index = this.allCart.findIndex((x) => x === prod);
      this.allCart[index].Cart = n * 1;
    }

    if (n <= 0) {
      this.removeFromCart(prod);
    }
    localStorage.setItem('cart', JSON.stringify(this.allCart));
    this.updateAmmount();
  }

  updateCartPriceItem(price, prod): number {
    const sum = price * prod;
    return parseFloat(sum.toFixed(2));
  }

  removeFromCart(prod): any {
    let cart = [];
    cart = JSON.parse(localStorage.getItem('cart'));

    for (const i of cart) {
      if (i.Id === prod.Id) {
        cart.splice(cart.indexOf(i), 1);
      }
    }
    localStorage.setItem('cart', JSON.stringify(cart));
    this.updateCart();
    this.updateAmmount();
  }

  updateCartItem(): void {
    if (localStorage.getItem('cart')) {
      let arr = [];

      arr = JSON.parse(localStorage.getItem('cart'));
      for (const item of arr) {
        this.productService.getOneProduct(item.Id).subscribe((product) => {
          if (item.Id && product == null) {
            for (const i of arr){
              if (i.Id === item.Id) {
                this.removeFromCart(arr[i]);
              }
            }
          }

          if (item.Id === product.Id) {
            if (item.Name !== product.Name || item.Price !== product.Price) {
              (item.Name = product.Name), (item.Price = product.Price);
              localStorage.removeItem('cart');
              localStorage.setItem('cart', JSON.stringify(arr));
            }
          }
        });
      }
    }
    this.updateCart();
  }

  priceToFloat(price): number {
    price.replace(/,/g, '.');
    return price.replace(/,/g, '.');
  }

  ngOnInit(): void {
    this.updateCartItem();
    this.updateCart();
    this.updateAmmount();
  }
}
