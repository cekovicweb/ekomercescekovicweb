import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ProductsService } from '../../_services/products.service';
import { Product } from 'src/app/_modles/product';
import { Category } from 'src/app/_modles/category';
import { CategoryService } from 'src/app/_services/category.service';
import { AuthService } from 'src/app/_services/-auth.service';
import { ImageService } from 'src/app/_services/image.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.sass'],
})
export class ProductDetailsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private productService: ProductsService,
    private categoryService: CategoryService,
    private imageService: ImageService,
    private location: Location,
    private authService: AuthService
  ) {}

  product: Product;
  categorys: Category[];
  public fileTypeCheck = false;
  public editProductForm = false;
  public editImageForm = false;
  public isMainChecbox = false;
  public token: any;
  public tokenId;
  public tokenInt: number;
  public file;

  ngOnInit(): void {
    this.getProduct();
    this.getCategorys();
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenInt = +this.tokenId;
    }
  }

  logged(): boolean {
    return this.authService.loggedIn();
  }

  goBack(): void {
    this.location.back();
  }

  getProduct(): void {
    const id = this.route.snapshot.paramMap.get('ID');
    this.productService
      .getOneProduct(id)
      .subscribe((product) => (this.product = product));
  }

  updateProduct(): void {
    const cat = this.product.CategoryId;
    const catInt: number = +cat;
    this.productService
      .updateProduct({
        Id: this.product.Id,
        Name: this.product.Name,
        Description: this.product.Description,
        Price: this.product.Price,
        IsMain: this.product.IsMain,
        Cart: this.product.Cart,
        CategoryId: catInt,
        UserId: this.tokenInt,
        ImageId: this.product.ImageId,
      } as Product)
      .subscribe(() => this.getProduct());
    this.showEditProductForm();
  }

  // show/hide form to edit product
  showEditProductForm(): void {
    this.editProductForm = !this.editProductForm;
  }
  showEditImageForm(): void {
    this.editImageForm = !this.editImageForm;
  }

  onFileChange(fileEvent): any[] {
    const file = fileEvent.target.files[0];
    const types = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];

    const fileType = file.type;
    let check: boolean;

    for (const type of types) {
      if (fileType === type) {
        check = true;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid #00dc00';
        return [(this.fileTypeCheck = check), (this.file = file)];
      } else {
        check = false;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid red';
      }
    }

    return [(this.fileTypeCheck = check)];
  }

  getCheckboxValue(e): void {
    this.isMainChecbox = e.target.checked;
  }

  addToCart(prod): any {
    return this.productService.addToCart(prod);
  }

  getCategorys(): any {
    this.categoryService
      .getCategorys()
      .subscribe((data) => (this.categorys = data));
  }

  updateImage(product: Product): void {
    const oldImage = product.Image;
    const formData = new FormData();
    formData.append('file', this.file, this.file.name);

    this.imageService.addImage(formData).subscribe((data) => {
      this.productService
        .updateProduct({
          Id: this.product.Id,
          Name: this.product.Name,
          Description: this.product.Description,
          Price: this.product.Price,
          IsMain: this.product.IsMain,
          Cart: this.product.Cart,
          CategoryId: this.product.CategoryId,
          UserId: this.tokenInt,
          ImageId: data.Id,
        } as Product)
        .subscribe(() =>
          this.imageService.deleteImage(oldImage).subscribe(() => {
            this.getProduct();
            this.showEditImageForm();
          })
        );
    });
  }

  priceToFloat(price): number {
    price.replace(/,/g, '.');
    return price.replace(/,/g, '.');
  }
}
