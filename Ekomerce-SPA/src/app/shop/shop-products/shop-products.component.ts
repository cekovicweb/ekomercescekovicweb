import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ProductsService } from '../../_services/products.service';
import { Observable } from 'rxjs';
import { Product } from 'src/app/_modles/product';

@Component({
  selector: 'app-shop-products',
  templateUrl: './shop-products.component.html',
  styleUrls: ['./shop-products.component.sass'],
})
export class ShopProductsComponent implements OnInit {
  baseUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private productService: ProductsService
  ) {
    if (localStorage.getItem('cart')) {
      this.productService.cart = JSON.parse(localStorage.getItem('cart'));
    }
  }
  products: Observable<Product[]>;

  ngOnInit(): void {
    this.get();
  }

  addToCart(prod): any {
    return this.productService.addToCart(prod);
  }

  get(): void {
    // this.productService
    //   .getProduct()
    //   .subscribe((data) => (this.products = data));
    this.products = this.productService.getProduct();
  }
  priceToFloat(price): number {
    price.replace(/,/g, '.');
    return price.replace(/,/g, '.');
  }
}
