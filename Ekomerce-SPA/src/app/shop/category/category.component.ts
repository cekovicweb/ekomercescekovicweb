import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/_modles/product';
import { map } from 'rxjs/operators';
import { ProductsService } from 'src/app/_services/products.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.sass'],
})
export class CategoryComponent implements OnInit {
  id: string;
  products: Observable<Product[]>;
  constructor(
    private route: ActivatedRoute,
    private productService: ProductsService
  ) {}

  ngOnInit(): any {
    // const queryParams = this.route.snapshot.queryParams
    // const routeParams = this.route.snapshot.params;

    // this.route.queryParams.subscribe(queryParams => {
    //   queryParams = this.getProducts()
    // })

    this.route.params.subscribe((routeParams) => {
      this.id = routeParams.id;
      this.getProducts();
    });
  }

  addToCart(prod): any {
    return this.productService.addToCart(prod);
  }

  getProducts(): any {
    // this.products = [];
    // this.productService.getProduct().subscribe((data) => {
    //   for (const prod of data) {
    //     if (prod.Category.NameCategory === this.id) {
    //       this.products.push(prod);
    //     }
    //   }
    // });
    this.products = this.productService.getProduct();
    this.products = this.products.pipe(map(data => data.filter(prod => prod.Category.NameCategory === this.id)));
  }
  priceToFloat(price): number {
    price.replace(/,/g, '.');
    return price.replace(/,/g, '.');
  }
}
