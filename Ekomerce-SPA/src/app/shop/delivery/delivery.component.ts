import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.sass'],
})
export class DeliveryComponent implements OnInit {
  public allCart = [];
  public ammount: number;
  public payment = false;
  public delivery = true;

  constructor() {
    this.updateCart();
    this.updateAmmount();
  }

  handleShowPayment(): void {
    this.payment = true;
    this.delivery = false;
  }

  handleShowDelivery(): void {
    this.delivery = true;
    this.payment = false;
  }

  updateCart(): any[] {
    return (this.allCart = JSON.parse(localStorage.getItem('cart')));
  }

  updateAmmount(): number {
    if (this.allCart) {
      this.ammount = 0.0;
      for (const prod of this.allCart) {
        this.ammount += prod.Cart * this.priceToFloat(prod.Price);
      }

      return (this.ammount = parseFloat(this.ammount.toFixed(2)));
    }
  }

  updateCartPriceItem(price, prod): number {
    const sum = price * prod;
    return parseFloat(sum.toFixed(2));
  }

  ngOnInit(): void {}

  priceToFloat(price): number {
    price.replace(/,/g, '.');
    return price.replace(/,/g, '.');
  }
}
