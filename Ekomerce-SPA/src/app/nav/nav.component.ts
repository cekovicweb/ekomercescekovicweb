import { Component, OnInit } from '@angular/core';
import { Category } from '../_modles/category';
import { CategoryService } from '../_services/category.service';
import {AuthService} from '../_services/-auth.service'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  constructor(private categoryService: CategoryService, private authService: AuthService) { }
  
  public token = this.authService.decodedToken
  public sidebar = false
  public categorys: Category[]

  ngOnInit() {
    this.getCategorys()
  }

  logged(): boolean{
    return this.authService.loggedIn()
  }

  logOut(): void{
    this.authService.logOut()
  }



  getCategorys(): any{
    this.categoryService.getCategorys().subscribe(data =>  this.categorys = data)
  }

  handleSidebarOpen(): void{
    this.sidebar = !this.sidebar
  }

  closeSidebar(): void {
    const checkbox = document.getElementById("check") as HTMLInputElement;
    checkbox.checked = false;
  }

  getLength(): number {
    if (localStorage.getItem('cart')) {
      let arr = []
      let count: number

      arr = JSON.parse(localStorage.getItem('cart'));
      count = 0
      for (let prod of arr) {
        count += prod.Cart*1
      }
      return count
    }
  }
}
