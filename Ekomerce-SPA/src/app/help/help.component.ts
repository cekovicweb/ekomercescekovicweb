import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css'],
})
export class HelpComponent implements OnInit {
  constructor() {}

  public item = false;
  public title = '';
  public content = '';
  showItem(id): void {
    switch (id) {
      case 1: {
        this.item = true;
        this.title = 'Jak dodać produkt?';
        this.content =
          'Wystarczy, że zalogujesz się do panelu użytkownika i wybierzesz zakładkę Produkty. Następnie wystaczy kliknąć przycisk Dodaj produkt i wypełnić wyświetlony formularz, a na sam koniec zatwierdzić operację przyciskiem Dodaj.';
        break;
      }
      case 2: {
        this.item = true;
        this.title = 'Jak usunąć produkt?';
        this.content =
          'Wystarczy, że zalogujesz się do panelu użytkownika i wybierzesz zakładkę Produkty. Wyświetli się lista wszystkich wystawionych przez Ciebie produktów i tam wystarczy kliknąć interesujący Cię produkt i następnie przycisk Usuń.';
        break;
      }
      case 3: {
        this.item = true;
        this.title = 'Czym są pakiety?';
        this.content =
          'Pakiety pozwalają na wybranie odpowiedniej usługi dla klienta tak, aby dopasować platformę do jego wymagań. Między innymi w pakiecie zawarta jest informacja ile serwisów może uruchomić użytkownik oraz jaki ma przydział dyskowy.';
        break;
      }
      case 4: {
        this.item = true;
        this.title = 'Jak zwiększyć miejsce na dysku?';
        this.content =
          'Dostępna przestrzeń dyskowa zależy od wybranego pakietu. Jeśli potrzebujesz więcej miejsca być może wystaczy przeorganizować zasoby wysłane na serwer, np. niepotrzebne usunąć. Natomiast jeśli nie przyniesie to zadowalającego rezultatu można wykupić większy pakiet.';
        break;
      }
    }
  }
  ngOnInit(): void {}
}
