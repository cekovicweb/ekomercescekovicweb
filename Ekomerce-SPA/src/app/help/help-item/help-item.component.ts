import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-help-item',
  templateUrl: './help-item.component.html',
  styleUrls: ['./help-item.component.css']
})
export class HelpItemComponent implements OnInit {

  constructor() { }
  @Input() public item: boolean;
  @Input() public title: string;
  @Input() public content: string;
  ngOnInit(): void {
  }

}
