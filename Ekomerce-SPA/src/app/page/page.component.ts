import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Page } from '../_modles/page';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {


  page: Page[];

  baseUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  getUsers(): Observable<Page[]> {
    return this.http.get<Page[]>(this.baseUrl + 'users');
  }

  getUser(id: number): Observable<Page[]> {
    return this.http.get<Page[]>(this.baseUrl + 'page' + id);
  }


}
