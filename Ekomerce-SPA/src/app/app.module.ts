import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MatSliderModule } from '@angular/material/slider';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageComponent } from './page/page.component';
import { NavComponent } from './nav/nav.component';
import { UserComponent } from './user/user.component';
import { LandingComponent } from './landing/landing.component';
import { HeaderComponent } from './landing/header/header.component';
import { AboutComponent } from './landing/about/about.component';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { PlansComponent } from './landing/plans/plans.component';
import { FooterComponent } from './landing/footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { BarComponent } from './user/bar/bar.component';
import { ProductsComponent } from './user/products/products.component';
import { SoldComponent } from './user/sold/sold.component';
import { PackagesComponent } from './user/packages/packages.component';
import { StorageComponent } from './user/storage/storage.component';
import { ShopComponent } from './shop/shop.component';
import { ShopProductsComponent } from './shop/shop-products/shop-products.component';
import { CartComponent } from './shop/cart/cart.component';
import { DeliveryComponent } from './shop/delivery/delivery.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoaderComponent } from './loader/loader.component';
import { HelpItemComponent } from './help/help-item/help-item.component';
import { HelpComponent } from './help/help.component';
import { CategoryComponent } from './shop/category/category.component';
import { FormsModule } from '@angular/forms';
import {NgxLocalStorageModule} from 'ngx-localstorage';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { ProductsService } from './_services/products.service';
import { ProductDetailsComponent } from './shop/product-details/product-details.component';
import { AuthService } from './_services/-auth.service';
import { AlertifyService } from './_services/alertify.service';
import { CategoryService } from './_services/category.service';
import { AccountTypeComponent } from './registration/account-type/account-type.component';
import { ArticlesComponent } from './user/articles/articles.component';
import { ServicesComponent } from './user/services/services.component';
import { ArticleDetailsComponent } from './user/articles/article-details/article-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BlogComponent } from './blog/blog.component';
import { ArticlesCarouselComponent } from './blog/articles-carousel/articles-carousel.component';
import {DropdownModule} from 'primeng/dropdown';

import { RippleModule } from 'primeng/ripple';
import {CarouselModule} from 'primeng/carousel';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import {CardModule} from 'primeng/card';
import {AvatarModule} from 'primeng/avatar';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {RatingModule} from 'primeng/rating';
import {DataViewModule} from 'primeng/dataview';
import {TableModule} from 'primeng/table';

import { UsersListComponent } from './users-list/users-list.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { WorkoutsComponent } from './user/dashboard/workouts/workouts.component';
import { DietsComponent } from './user/dashboard/diets/diets.component';
import { CalendarComponent } from './user/dashboard/calendar/calendar.component';
import { YourUsersComponent } from './user/dashboard/your-users/your-users.component';
import { CoachPackageComponent } from './user/dashboard/coach-package/coach-package.component';
import { YourUsersDetailsComponent } from './user/dashboard/your-users/your-users-details/your-users-details.component';
import { UserWorkoutsComponent } from './user/user-workouts/user-workouts.component';
import { UserDietsComponent } from './user/user-diets/user-diets.component';
import { UserMedicineComponent } from './user/user-medicine/user-medicine.component';
import { MedicineComponent } from './user/dashboard/medicine/medicine.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import localePl from '@angular/common/locales/pl';
import {registerLocaleData} from '@angular/common';

registerLocaleData(localePl);



@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    NavComponent,
    UserComponent,
    LandingComponent,
    HeaderComponent,
    AboutComponent,
    PlansComponent,
    FooterComponent,
    ContactComponent,
    BarComponent,
    ProductsComponent,
    SoldComponent,
    PackagesComponent,
    StorageComponent,
    ShopComponent,
    ShopProductsComponent,
    CartComponent,
    DeliveryComponent,
    HelpComponent,
    HelpItemComponent,
    LoaderComponent,
    CategoryComponent,
    RegistrationComponent,
    LoginComponent,
    ProductDetailsComponent,
    AccountTypeComponent,
    ArticlesComponent,
    ServicesComponent,
    ArticleDetailsComponent,
    BlogComponent,
    ArticlesCarouselComponent,
    UsersListComponent,
    DashboardComponent,
    WorkoutsComponent,
    DietsComponent,
    CalendarComponent,
    YourUsersComponent,
    CoachPackageComponent,
    YourUsersDetailsComponent,
    UserWorkoutsComponent,
    UserDietsComponent,
    UserMedicineComponent,
    MedicineComponent,
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    Ng2PageScrollModule,
    BrowserAnimationsModule,
    MatSliderModule,
    FormsModule,
    NgxLocalStorageModule.forRoot(),
    ReactiveFormsModule,

    RippleModule,
    CarouselModule,
    ButtonModule,
    ToastModule,
    CardModule,
    AvatarModule,
    ToggleButtonModule,
    RatingModule,
    DropdownModule,
    DataViewModule,
    TableModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    NgbModule,

    // SkeletonModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [ProductsService, CategoryService, AuthService, AlertifyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
