import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MDBBootstrapModule, NavbarComponent } from 'angular-bootstrap-md';
import { AppComponent } from './app.component';
import { HelpItemComponent } from './help/help-item/help-item.component';
import { HelpComponent } from './help/help.component';
import { AboutComponent } from './landing/about/about.component';
import { FooterComponent } from './landing/footer/footer.component';
import { HeaderComponent } from './landing/header/header.component';
import { PlansComponent } from './landing/plans/plans.component';
import { LoaderComponent } from './loader/loader.component';
import { PackagesComponent } from './user/packages/packages.component';
import { ProductsComponent } from './user/products/products.component';
import { SoldComponent } from './user/sold/sold.component';
import { StorageComponent } from './user/storage/storage.component';
import { UserComponent } from './user/user.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MDBBootstrapModule.forRoot()
      ],
      declarations: [
        AppComponent,
        FooterComponent,
        LoaderComponent,
        ProductsComponent,
        SoldComponent,
        PackagesComponent,
        StorageComponent,
        NavbarComponent,
        AboutComponent,
        HeaderComponent,
        PlansComponent,
        HelpComponent,
        HelpItemComponent,
        UserComponent
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Ekomerce-SPA'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Ekomerce-SPA');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('Ekomerce-SPA app is running!');
  });
});
