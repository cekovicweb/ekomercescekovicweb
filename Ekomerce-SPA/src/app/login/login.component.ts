import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/-auth.service';
import { AlertifyService } from '../_services/alertify.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
})
export class LoginComponent implements OnInit {
  model: any = {};

  constructor(
    private authService: AuthService,
    private alertify: AlertifyService,
    public router: Router
  ) {}

  login(user: string, password: string): void {
    this.authService.login({ UserName: user, Password: password }).subscribe(
      () => {
        this.router.navigate(['user']);
        this.alertify.success('Zalogowany!');
      },
      (error) => {
        this.alertify.error(error.error);
      }
    );
  }

  ngOnInit(): void {}
}
