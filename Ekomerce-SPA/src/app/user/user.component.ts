import { User } from './../_modles/user';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from '../_services/-auth.service';
import { ImageService } from '../_services/image.service';
import { CoachPackageService } from '../_services/coach-package.service';
import { CoachPackage } from '../_modles/coachPackage';
import { PhysiotherapistPackage } from '../_modles/physiotherapist';
import { PhysiotherapistPackageService } from '../_services/physiotherapist-package.service';
import { DoctorPackage } from '../_modles/doctorPackage';
import { DoctorPackageService } from '../_services/doctor-package.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.sass'],
})
export class UserComponent implements OnInit {
  // user: User;
  // baseUrl =  environment.apiUrl;
  public editImageForm = false;
  public fileTypeCheck = false;
  public file;
  public button1 = false;
  public button2 = false;
  public button3 = false;
  public button4 = false;
  public button5 = false;
  public button6 = false;
  public diets = false;
  public workouts = false;
  public medicine = false;
  // public button7 = false;
  public package = 'BRAK';
  public user: User;
  public token: any = this.auth.getTokenId();
  public account;
  public coachPackage: CoachPackage;
  public physiotherapistPackage: PhysiotherapistPackage;
  public doctorPackage: DoctorPackage;

  showProducts(): void {
    this.button1 = !this.button1;
    this.button2 = false;
    this.button3 = false;
    this.button4 = false;
    this.button5 = false;
    this.button6 = false;
    // this.button7 = false;
  }
  showSold(): void {
    this.button2 = !this.button2;
    this.button1 = false;
    this.button3 = false;
    this.button4 = false;
    this.button5 = false;
    this.button6 = false;
    // this.button7 = false;
  }
  showPackages(): void {
    this.button3 = !this.button3;
    this.button1 = false;
    this.button2 = false;
    this.button4 = false;
    this.button5 = false;
    this.button6 = false;
    // this.button7 = false;
  }
  showStorage(): void {
    this.button4 = !this.button4;
    this.button1 = false;
    this.button2 = false;
    this.button3 = false;
    this.button5 = false;
    this.button6 = false;
    // this.button7 = false;
  }
  showArticle(): void {
    this.button5 = !this.button5;
    this.button1 = false;
    this.button2 = false;
    this.button3 = false;
    this.button4 = false;
    this.button6 = false;
    // this.button7 = false;
  }
  showServices(): void {
    this.button6 = !this.button6;
    this.button1 = false;
    this.button2 = false;
    this.button3 = false;
    this.button4 = false;
    this.button5 = false;
    // this.button7 = false;
  }
  showDiets(): void {
    this.diets = !this.diets;
    this.workouts = false;
    this.medicine = false;
  }
  showWorkouts(): void {
    this.diets = false;
    this.workouts = !this.workouts;
    this.medicine = false;
  }
  showMedicine(): void {
    this.diets = false;
    this.workouts = false;
    this.medicine = !this.medicine;
  }
  // showDashboard(): void {
  //   this.button7 = !this.button7;
  //   this.button1 = false;
  //   this.button2 = false;
  //   this.button3 = false;
  //   this.button4 = false;
  //   this.button5 = false;
  //   this.button6 = false;
  // }

  showEditImageForm(): void {
    this.editImageForm = !this.editImageForm;
  }

  constructor(
    private http: HttpClient,
    private auth: AuthService,
    private imageService: ImageService,
    private packageSevvice: CoachPackageService,
    private physiotherapistPackageService: PhysiotherapistPackageService,
    private doctorPackageService: DoctorPackageService
  ) {}

  getPackage(pack: string): any {
    this.package = pack;
  }

  getCoachPackage(id: number): void {
    if (id !== null) {
      this.packageSevvice
        .getPackage(id)
        .subscribe((data) => (this.coachPackage = data));
    }
  }
  getPhisiotherapistPackage(id: number): void {
    if (id !== null) {
      this.physiotherapistPackageService
        .getPackage(id)
        .subscribe((data) => (this.physiotherapistPackage = data));
    }
  }
  getDoctorPackage(id: number): void {
    if (id !== null) {
      this.doctorPackageService
        .getPackage(id)
        .subscribe((data) => (this.doctorPackage = data));
    }
  }

  getUser(id: any): void {
    this.auth.getUser(id).subscribe((data: User) => {
      this.user = {
        Id: data.Id,
        Username: data.Username,
        Name: data.Name,
        LastName: data.LastName,
        Gender: data.Gender,
        DateOfBirth: new Date(data.DateOfBirth).toISOString().substring(0, 10),
        ZodiacSign: data.ZodiacSign,
        Created: new Date(data.Created).toISOString().substring(0, 10),
        LastActive: new Date().toISOString().substring(0, 10),
        City: data.City,
        Country: data.Country,
        AccountType: data.AccountType,
        RateUsers: data.RateUsers,
        ImageId: data.ImageId,
        Image: data.Image,
        CoachId: data.CoachId,
        CoachPackageId: data.CoachPackageId,
        DietPlans: data.DietPlans,
        TrainingPlans: data.TrainingPlans,
        PhysiotherapistId: data.PhysiotherapistId,
        PhysiotherapistPackageId: data.PhysiotherapistPackageId,
        DoctorId: data.DoctorId,
        DoctorPackageId: data.DoctorPackageId,
      };
      // console.log(data.PhysiotherapistId);
      this.getCoachPackage(data.CoachPackageId);
      this.getPhisiotherapistPackage(data.PhysiotherapistPackageId);
      this.getDoctorPackage(data.DoctorPackageId);
    });
  }

  onFileChange(fileEvent): any[] {
    const file = fileEvent.target.files[0];
    const types = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];

    const fileType = file.type;
    let check: boolean;

    for (const type of types) {
      if (fileType === type) {
        check = true;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid #00dc00';
        return [(this.fileTypeCheck = check), (this.file = file)];
      } else {
        check = false;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid red';
      }
    }

    return [(this.fileTypeCheck = check)];
  }

  updateImage(user: User): void {
    const oldImage = user.Image;
    const formData = new FormData();
    formData.append('file', this.file, this.file.name);

    if (user.ImageId == null) {
      this.imageService.addImage(formData).subscribe((data) => {
        this.auth
          .updateUser({
            Id: user.Id,
            ImageId: data.Id,
            CoachId: user.CoachId,
            CoachPackageId: user.CoachPackageId,
            PhysiotherapistId: user.PhysiotherapistId,
            PhysiotherapistPackageId: user.PhysiotherapistPackageId,
            DoctorId: user.DoctorId,
            DoctorPackageId: user.DoctorPackageId,
          } as User)
          .subscribe(() => {
            this.ngOnInit();
            this.showEditImageForm();
          });
      });
    } else if (user.ImageId != null) {
      this.imageService.addImage(formData).subscribe((data) => {
        this.auth
          .updateUser({
            Id: user.Id,
            ImageId: data.Id,
            CoachId: user.CoachId,
            CoachPackageId: user.CoachPackageId,
            PhysiotherapistId: user.PhysiotherapistId,
            PhysiotherapistPackageId: user.PhysiotherapistPackageId,
            DoctorId: user.DoctorId,
            DoctorPackageId: user.DoctorPackageId,
          } as User)
          .subscribe(() =>
            this.imageService.deleteImage(oldImage).subscribe(() => {
              this.ngOnInit();
              this.showEditImageForm();
            })
          );
      });
    }
  }

  ngOnInit(): void {
    this.getUser(this.token.nameid);
  }
}
