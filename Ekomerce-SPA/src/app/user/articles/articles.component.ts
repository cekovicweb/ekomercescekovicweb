import { formatCurrency } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProductDetailsComponent } from 'src/app/shop/product-details/product-details.component';
import { Article } from 'src/app/_modles/article';
import { AuthService } from 'src/app/_services/-auth.service';
import { ArticleService } from 'src/app/_services/article.service';
import { ImageService } from 'src/app/_services/image.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.sass'],
})
export class ArticlesComponent implements OnInit {
  @Input() public button5: boolean;
  public addArticleForm = false;
  articles = [];
  showTime = false;
  public token: any;
  public tokenId;
  public tokenIdInt: number;
  public file;
  public fileTypeCheck = false;

  constructor(
    private articleService: ArticleService,
    private authService: AuthService,
    private imageService: ImageService
  ) {}

  ngOnInit(): void {
    this.getArticles();
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
    }
  }

  logged(): boolean {
    return this.authService.loggedIn();
  }

  getCheckboxValue(e): void {
    this.showTime = e.target.checked;
  }

  showAddArticleForm(): void {
    this.addArticleForm = !this.addArticleForm;
  }

  getArticles(): void {
    this.articles = [];
    this.articleService.getArticles().subscribe((data) => {
      for (const article of data) {
        if (article.UserId === this.tokenIdInt) {
          this.articles.push(article);
        }
      }
    });
  }

  onFileChange(fileEvent): any[] {
    const file = fileEvent.target.files[0];
    const types = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];

    const fileType = file.type;
    let check: boolean;
    for (const type of types) {
      if (fileType === type) {
        check = true;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid #00dc00';
        return [(this.fileTypeCheck = check), (this.file = file)];
      } else {
        check = false;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid red';
      }
    }

    return [(this.fileTypeCheck = check)];
  }

  addArticle(
    title: string,
    shortDesc: string,
    longDesc: string,
    form: NgForm
  ): void {
    const date = new Date();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    let newDay: string;
    let newMonth: string;

    if (day < 10) {
      newDay = '0' + day.toString();
    } else {
      newDay = day.toString();
    }
    if (month < 10) {
      newMonth = '0' + month.toString();
    } else {
      newMonth = month.toString();
    }

    const hour = date.getHours();
    const minute = date.getMinutes();
    const secound = date.getSeconds();
    let newHour: string;
    let newMinute: string;
    let newSecound: string;

    if (hour < 10) {
      newHour = '0' + hour.toString();
    } else {
      newHour = hour.toString();
    }
    if (minute < 10) {
      newMinute = '0' + minute.toString();
    } else {
      newMinute = minute.toString();
    }
    if (secound < 10) {
      newSecound = '0' + secound.toString();
    } else {
      newSecound = secound.toString();
    }
    const formData = new FormData();
    formData.append('file', this.file, this.file.name);
    this.imageService.addImage(formData).subscribe((data) => {
      this.articleService
        .addArticle({
          Title: title.toString(),
          ShortDesc: shortDesc.toString(),
          LongDesc: longDesc.toString(),

          Date: newDay + '.' + newMonth + '.' + date.getFullYear().toString(),
          Time: newHour + ':' + newMinute + ':' + newSecound,
          ShowTime: this.showTime,
          UserId: this.tokenIdInt,
          ImageId: data.Id,
        } as Article)
        .subscribe((article) => {
          this.articles.push(article);
          this.getArticles();
        });
      form.reset();
      this.showAddArticleForm();
    });
  }

  deleteArticle(article: Article): void {
    this.articles = this.articles.filter((a) => a.Id !== article.Id);
    this.articleService.deleteArticle(article).subscribe();
    this.imageService.deleteImage(article.Image).subscribe();
  }
}
