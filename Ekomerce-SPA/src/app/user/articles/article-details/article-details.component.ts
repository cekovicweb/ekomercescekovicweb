import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'src/app/_modles/article';
import { Rate } from 'src/app/_modles/rate';
import { RateUser } from 'src/app/_modles/rateUser';
import { AuthService } from 'src/app/_services/-auth.service';
import { ArticleService } from 'src/app/_services/article.service';
import { ImageService } from 'src/app/_services/image.service';
import { RateService } from 'src/app/_services/rate.service';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.sass'],
})
export class ArticleDetailsComponent implements OnInit {
  article: Article;
  public showTime: boolean = false;
  public editArticleForm: boolean = false;
  public editImageForm: boolean = false;
  public token: any;
  public tokenId;
  public tokenIdInt: number;
  public file;
  public fileTypeCheck: boolean = false;
  public rate: number = 0;
  public rateUser: number = 0;

  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleService,
    private authService: AuthService,
    private imageService: ImageService,
    private rateService: RateService
  ) {}

  ngOnInit(): void {
    this.getArticle();
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
    }
  }

  onFileChange(fileEvent) {
    const file = fileEvent.target.files[0];
    const types = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];

    var fileType = file.type;
    var check: boolean;
    for (let type of types) {
      if (fileType == type) {
        check = true;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid #00dc00';
        return [(this.fileTypeCheck = check), (this.file = file)];
      } else {
        check = false;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid red';
      }
    }

    return (this.fileTypeCheck = check);
  }

  logged(): boolean {
    return this.authService.loggedIn();
  }

  addRate(rate: number, article: Article): void {
    this.rateService
      .addRate({
        RateValue: rate,
        UserId: this.tokenIdInt,
        ArticleId: article.Id,
      } as Rate)
      .subscribe(() => {
        this.canRate(article);
        this.getArticle();
      });
  }

  addUserRate(rateUser: number, article: Article): void {
    this.rateService
      .addUserRate({
        RateValue: rateUser,
        UserId: article.UserId,
        RateUserId: this.tokenIdInt,
      } as RateUser)
      .subscribe(() => {
        this.canRateUser(article);
        this.getArticle();
      });
  }

  canRate(article: Article): boolean {
    if(this.tokenIdInt == article.UserId){
      return false;
    }
    for (let ra of article.Rates) {
      if (this.tokenIdInt == ra.UserId) {
        this.rate = ra.RateValue;
        return false;
      }
    }
    return true;
  }

  canRateUser(article: Article): boolean {
    if(this.tokenIdInt == article.UserId){
      return false;
    }
    for (let ra of article.User.RateUsers) {
      if (this.tokenIdInt == ra.RateUserId) {
        this.rateUser = ra.RateValue;
        return false;
      }
    }
    return true;
  }

  getArticle(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.articleService
      .getArticle(id)
      .subscribe((data) => (this.article = data));
  }

  updateArticle(): void {
    this.articleService
      .updateArticle(this.article)
      .subscribe(() => this.showEditArticleForm());
  }

  updateImage(article: Article) {
    var oldImage = article.Image;
    const formData = new FormData();
    formData.append('file', this.file, this.file.name);

    this.imageService.addImage(formData).subscribe((data) => {
      this.articleService
        .updateArticle({
          Id: article.Id,
          Date: article.Date,
          ShortDesc: article.ShortDesc,
          LongDesc: article.LongDesc,
          ShowTime: article.ShowTime,
          Time: article.Time,
          Title: article.Title,
          UserId: article.UserId,
          ImageId: data.Id,
        } as Article)
        .subscribe(() =>
          this.imageService.deleteImage(oldImage).subscribe(() => {
            this.getArticle();
            this.showEditImageForm();
          })
        );
    });
  }

  showEditArticleForm(): void {
    this.editArticleForm = !this.editArticleForm;
  }

  showEditImageForm() {
    this.editImageForm = !this.editImageForm;
  }
}
