import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Service } from 'src/app/_modles/service';
import { ServiceService } from 'src/app/_services/service.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/_services/-auth.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.sass'],
})
export class ServicesComponent implements OnInit {
  public addServiceForm = false;
  public edit = false;
  @Input() public button6: boolean;
  services = [];
  public token: any = this.authService.getTokenId();
  public tokenId = this.token.nameid;
  public tokenIdInt: number = +this.tokenId;

  form = new FormGroup({
    id: new FormControl(''),
    name: new FormControl('', Validators.required),
    price: new FormControl(''),
    userId: new FormControl(''),
  });

  constructor(
    private serviceService: ServiceService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.getServices();
  }

  showAddServiceForm(): void {
    this.addServiceForm = !this.addServiceForm;
    this.edit = false;
    this.form.reset();
  }

  getServices(): void {
    this.services = [];
    this.serviceService.getServices().subscribe((data) => {
      for (const service of data) {
        if (service.UserId === this.tokenIdInt) {
          this.services.push(service);
        }
      }
    });
  }


  addService(): void {
    this.serviceService
      .addService({
        ServiceName: this.form.value.name.toString(),
        Price: this.form.value.price.toString(),
        UserId: this.tokenIdInt,
      } as Service)
      .subscribe((service) => {
        this.services.push(service);
      });
    this.form.reset();
    this.showAddServiceForm();
  }

  showEditForm(service: Service): void {
    this.edit = true;
    this.addServiceForm = !this.addServiceForm;
    this.form.setValue({
      id: service.Id,
      name: service.ServiceName,
      price: service.Price,
      userId: service.UserId,
    });
  }

  updateService(): void {
    this.serviceService
      .updateService({
        Id: this.form.value.id,
        ServiceName: this.form.value.name,
        Price: this.form.value.price,
        UserId: this.form.value.userId,
      } as Service)
      .subscribe(() => this.getServices());
    this.addServiceForm = false;
  }

  deleteService(service: Service): void {
    this.services = this.services.filter((a) => a.Id !== service.Id);
    this.serviceService.deleteService(service).subscribe();
  }
}
