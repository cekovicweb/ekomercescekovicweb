import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.sass']
})
export class PackagesComponent implements OnInit {

  constructor() { }
  @Input() public button3: boolean;
  @Output() packageBtn = new EventEmitter<any>();
  setPackage(name: string): any{
    this.packageBtn.emit(name);
  }
  ngOnInit(): void {
  }

}
