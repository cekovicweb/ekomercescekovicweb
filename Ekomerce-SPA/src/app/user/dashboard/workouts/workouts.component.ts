import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Training } from 'src/app/_modles/training';
import { TrainingExercise } from 'src/app/_modles/trainingExercise';
import { TrainingPlan } from 'src/app/_modles/trainingPlan';
import { AuthService } from 'src/app/_services/-auth.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ImageService } from 'src/app/_services/image.service';
import { TrainingService } from 'src/app/_services/training.service';

@Component({
  selector: 'app-workouts',
  templateUrl: './workouts.component.html',
  styleUrls: ['./workouts.component.sass'],
})
export class WorkoutsComponent implements OnInit {
  @Input() public userID: number;
  @Input() public coachID: number;
  @Input() public trainingPlans: TrainingPlan[];
  public trainings: Training[];
  public trainingExercises: TrainingExercise[];
  public addTrainingPlanForm = false;
  public addTrainingForm = false;
  public addExerciseForm = false;
  public file;
  public fileTypeCheck = false;
  public editTraining = false;
  public editExercise = false;

  time = new FormControl('');

  trainingId = new FormControl('');
  trainingplan = new FormControl('');
  calorific = new FormControl('');
  duration = new FormControl('');
  trainingname = new FormControl('');

  exerciseId = new FormControl('');
  training = new FormControl('');
  exercisename = new FormControl('');
  durationEx = new FormControl('');
  repeat = new FormControl('');
  imageId = new FormControl('');

  form = new FormGroup({
    time: this.time,
  });
  form2 = new FormGroup({
    trainingId: this.trainingId,
    trainingplan: this.trainingplan,
    calorific: this.calorific,
    duration: this.duration,
    trainingname: this.trainingname,
  });
  form3 = new FormGroup({
    exerciseId: this.exerciseId,
    exercisename: this.exercisename,
    durationEx: this.durationEx,
    repeat: this.repeat,
    imageId: this.imageId,
    training: this.training,
    // price: this.priceforweight,
  });

  constructor(
    private alertifyService: AlertifyService,
    private trainingService: TrainingService,
    private imageService: ImageService,
    private authService: AuthService
  ) {}

  showAddForm(id: number): void {
    switch (id) {
      case 1: {
        this.addTrainingPlanForm = !this.addTrainingPlanForm;
        this.addTrainingForm = false;
        this.addExerciseForm = false;
        break;
      }
      case 2: {
        if (this.editTraining) {
          this.editTraining = false;
        } else {
          this.addTrainingPlanForm = false;
          this.addTrainingForm = !this.addTrainingForm;
          this.addExerciseForm = false;
        }
        this.form2.reset();
        this.editExercise = false;
        break;
      }
      case 3: {
        if (this.editExercise) {
          this.editExercise = false;
        } else {
          this.addTrainingPlanForm = false;
          this.addTrainingForm = false;
          this.addExerciseForm = !this.addExerciseForm;
        }
        this.form3.reset();
        this.editTraining = false;
        break;
      }
      default: {
        this.addTrainingPlanForm = false;
        this.addTrainingForm = false;
        this.addExerciseForm = false;
        this.editTraining = false;
        this.editExercise = false;
        break;
      }
    }
  }

  showEditTrainingForm(training: Training): void {
    if (this.addTrainingForm && this.editTraining) {
      this.editTraining = true;
      this.form2.setValue({
        trainingId: training.Id,
        trainingplan: training.TrainingPlanId,
        calorific: training.Calorific,
        duration: training.Duration,
        trainingname: training.TrainingName,
      });
    }  else if (!this.addTrainingForm && !this.editTraining){
      this.addTrainingForm = !this.addTrainingForm;
      this.editTraining = true;
      this.form2.setValue({
        trainingId: training.Id,
        trainingplan: training.TrainingPlanId,
        calorific: training.Calorific,
        duration: training.Duration,
        trainingname: training.TrainingName,
      });
    }else if (!this.addTrainingForm) {
      this.addTrainingForm = !this.addTrainingForm;
    }
  }

  showEditExerciseForm(exercise: TrainingExercise): void {
    if (!this.addExerciseForm || this.editExercise) {
      this.addExerciseForm = !this.addExerciseForm;
    }
    this.editExercise = true;
    this.form3.setValue({
      exerciseId: exercise.Id,
      exercisename: exercise.ExerciseName,
      durationEx: exercise.Duration,
      repeat: exercise.Reapeat,
      imageId: exercise.ImageId,
      training: exercise.TrainingId,
    });
  }

  addTrainingPlan(): void {
    this.trainingService
      .addTrainingPlan({
        Time: this.form.value.time.toString(),
        UserId: this.userID,
        CoachId: this.coachID,
      } as TrainingPlan)
      .subscribe((plan) => {
        this.trainingPlans.push(plan);
        this.form.reset();
        this.showAddForm(0);
        this.alertifyService.success(
          'Plan dodany! Możesz dodać trening do planu.'
        );
      });
  }

  addTraining(): void {
    this.trainingService
      .addTraining({
        TrainingName: this.form2.value.trainingname,
        Calorific: this.form2.value.calorific,
        Duration: this.form2.value.duration,
        TrainingPlanId: this.form2.value.trainingplan,
      } as Training)
      .subscribe((training) => {
        this.trainings.push(training);
        this.form2.reset();
        this.showAddForm(0);
        this.alertifyService.success(
          'Trening dodany! Możesz dodać ćwiczenia do treningu.'
        );
      });
  }

  updateTraining(): void {
    this.trainingService
      .updateTraining({
        Id: this.form2.value.trainingId,
        TrainingName: this.form2.value.trainingname,
        Calorific: this.form2.value.calorific,
        Duration: this.form2.value.duration,
        TrainingPlanId: this.form2.value.trainingplan,
      } as Training)
      .subscribe(() => {
        this.addTrainingForm = false;
        this.ngOnInit();
        this.alertifyService.success('Trening zaktualizowany! Odśwież stronę.');
      });
  }
  updateExercise(): void {
    this.trainingService
      .updateExercise({
        Id: this.form3.value.exerciseId,
        ExerciseName: this.form3.value.exercisename,
        Duration: this.form3.value.durationEx,
        Reapeat: this.form3.value.repeat,
        TrainingId: this.form3.value.training,
        ImageId: this.form3.value.imageId
      } as TrainingExercise)
      .subscribe(() => {
        this.addExerciseForm = false;
        this.ngOnInit();
        this.alertifyService.success('Ćwiczenie zaktualizowane! Odśwież stronę.');
      });
  }

  addExercise(): void {
    const formData = new FormData();
    formData.append('file', this.file, this.file.name);
    this.imageService.addImage(formData).subscribe((data) => {
      this.trainingService
        .addExercise({
          ExerciseName: this.form3.value.exercisename,
          Duration: this.form3.value.durationEx,
          Reapeat: this.form3.value.repeat.toString(),
          TrainingId: this.form3.value.training,
          ImageId: data.Id,
        } as TrainingExercise)
        .subscribe((exercise) => {
          this.trainingExercises.push(exercise);
          this.form3.reset();
          this.showAddForm(0);
          this.alertifyService.success('Ćwiczenie dodane!');
        });
    });
  }

  // method to validate form input file
  onFileChange(fileEvent): any[] {
    const file = fileEvent.target.files[0];
    const types = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];
    const fileType = file.type;
    let check: boolean;
    for (const type of types) {
      if (fileType === type) {
        check = true;
        document.getElementById('imageId').style.borderBottom =
          '2px solid #00dc00';
        return [(this.fileTypeCheck = check), (this.file = file)];
      } else {
        check = false;
        document.getElementById('imageId').style.borderBottom = '2px solid red';
      }
    }

    return [(this.fileTypeCheck = check)];
  }

  ngOnInit(): void {
    this.trainings = [];
    if (this.trainingPlans.length > 0) {
      this.trainingPlans.forEach((element) => {
        if (element.Trainings.length > 0) {
          element.Trainings.forEach((data) => {
            this.trainings.push(data);
          });
        }
      });
      this.trainings.forEach((e) => {
        this.trainingExercises = e.TrainingExercises;
      });
    }
  }
}
