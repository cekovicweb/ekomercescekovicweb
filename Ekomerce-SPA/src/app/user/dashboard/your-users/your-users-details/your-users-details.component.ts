import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CoachPackage } from 'src/app/_modles/coachPackage';
import { PhysiotherapistPackage } from 'src/app/_modles/physiotherapist';
import { User } from 'src/app/_modles/user';
import { AuthService } from 'src/app/_services/-auth.service';
import { CoachPackageService } from 'src/app/_services/coach-package.service';
import { PhysiotherapistPackageService } from 'src/app/_services/physiotherapist-package.service';

@Component({
  selector: 'app-your-users-details',
  templateUrl: './your-users-details.component.html',
  styleUrls: ['./your-users-details.component.sass'],
})
export class YourUsersDetailsComponent implements OnInit {
  public user: User;
  public coachPackage: CoachPackage;
  public physioPackage: PhysiotherapistPackage;
  public loggedUser: User;
  public token: any;
  public tokenId: any;
  public tokenIdInt: number;
  public id: string;
  public workouts = false;
  public diets = false;
  public medicine = false;
  // public userId;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private coachPackageService: CoachPackageService,
    private physiotherapistPackageService: PhysiotherapistPackageService
  ) {}

  logged(): boolean {
    return this.authService.loggedIn();
  }

  showComponent(id: number): void {
    switch (id) {
      case 1: {
        this.workouts = !this.workouts;
        this.diets = false;
        this.medicine = false;
        break;
      }
      case 2: {
        this.workouts = false;
        this.diets = !this.diets;
        this.medicine = false;
        break;
      }
      case 3: {
        this.workouts = false;
        this.diets = false;
        this.medicine = !this.medicine;
        break;
      }
    }
  }

  getUser(id: string): void {
    this.authService.getUser(id).subscribe((data) => {
      this.user = data;
      if (data.CoachPackageId !== null) {
        this.getCoachPackage(data.CoachPackageId);
      }
      if (data.PhysiotherapistPackageId !== null) {
        this.getPhysioPackage(data.PhysiotherapistPackageId);
      }
    });
  }

  getCoachPackage(id: number): void {
    this.coachPackageService
      .getPackage(id)
      .subscribe((data) => (this.coachPackage = data));
  }
  getPhysioPackage(id: number): void {
    this.physiotherapistPackageService
      .getPackage(id)
      .subscribe((data) => (this.physioPackage = data));
  }

  getLoggedUser(): void {
    this.authService.getUser(this.tokenIdInt).subscribe((user) => {
      this.loggedUser = user;
    });
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getUser(this.id);
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
      this.getLoggedUser();
    }
  }
}
