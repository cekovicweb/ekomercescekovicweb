import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourUsersDetailsComponent } from './your-users-details.component';

describe('YourUsersDetailsComponent', () => {
  let component: YourUsersDetailsComponent;
  let fixture: ComponentFixture<YourUsersDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YourUsersDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YourUsersDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
