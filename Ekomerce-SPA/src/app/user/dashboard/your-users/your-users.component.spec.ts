import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourUsersComponent } from './your-users.component';

describe('YourUsersComponent', () => {
  let component: YourUsersComponent;
  let fixture: ComponentFixture<YourUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YourUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YourUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
