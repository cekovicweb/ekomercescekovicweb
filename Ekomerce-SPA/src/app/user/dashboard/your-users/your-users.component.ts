import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_modles/user';
import { AuthService } from 'src/app/_services/-auth.service';

@Component({
  selector: 'app-your-users',
  templateUrl: './your-users.component.html',
  styleUrls: ['./your-users.component.sass'],
})
export class YourUsersComponent implements OnInit {
  public yourUsers = false;
  public workouts = false;
  public diets = false;
  public calendar = false;
  public loggedUser: User;
  public users: User[] = [];
  public token: any;
  public tokenId: any;
  public tokenIdInt: number;

  constructor(private authService: AuthService) {}

  logged(): boolean {
    return this.authService.loggedIn();
  }

  showComponent(id: number): void {
    switch (id) {
      case 0: {
        this.yourUsers = !this.yourUsers;
        this.workouts = false;
        this.diets = false;
        this.calendar = false;
        break;
      }
      case 1: {
        this.yourUsers = false;
        this.workouts = !this.workouts;
        this.diets = false;
        this.calendar = false;
        break;
      }
      case 2: {
        this.yourUsers = false;
        this.workouts = false;
        this.diets = !this.diets;
        this.calendar = false;
        break;
      }
      case 3: {
        this.yourUsers = false;
        this.workouts = false;
        this.diets = false;
        this.calendar = !this.calendar;
        break;
      }
    }
  }

  getUsers(coachphysId: number): void {
    if (this.loggedUser?.AccountType === 'trener') {
      this.authService.getUsers().subscribe((data) => {
        this.users = data;
        this.users = this.users.filter((user) => user.CoachId === coachphysId);
      });
    } else if (this.loggedUser?.AccountType === 'Fizjoterapeuta') {
      this.authService.getUsers().subscribe((data) => {
        this.users = data;
        this.users = this.users.filter(
          (user) => user.PhysiotherapistId === coachphysId
        );
      });
    }
  }

  getLoggedUser(): void {
    this.authService.getUser(this.tokenIdInt).subscribe((user) => {
      this.loggedUser = user;
      this.getUsers(user.Id);
    });
  }

  ngOnInit(): void {
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
      this.getLoggedUser();
    }
  }
}
