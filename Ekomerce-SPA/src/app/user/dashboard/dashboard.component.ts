import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/_modles/user';
import { AuthService } from 'src/app/_services/-auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {

  constructor(private authService: AuthService) { }

  public yourUsers = false;
  public workouts = false;
  public diets = false;
  public calendar = false;
  public packages = false;
  @Input() public button7;
  public token: any;
  public tokenId: any;
  public tokenIdInt: number;
  public user: User;

  logged(): boolean {
    return this.authService.loggedIn();
  }

  showComponent(id: number): void{
    switch (id) {
      case 0: {
        this.yourUsers = !this.yourUsers;
        this.workouts = false;
        this.diets = false;
        this.calendar = false;
        this.packages = false;
        break;
      }
      case 1: {
        this.yourUsers = false;
        this.workouts = !this.workouts;
        this.diets = false;
        this.calendar = false;
        this.packages = false;
        break;
      }
      case 2: {
        this.yourUsers = false;
        this.workouts = false;
        this.diets = !this.diets;
        this.calendar = false;
        this.packages = false;
        break;
      }
      case 3: {
        this.yourUsers = false;
        this.workouts = false;
        this.diets = false;
        this.calendar = !this.calendar;
        this.packages = false;
        break;
      }
      case 4: {
        this.yourUsers = false;
        this.workouts = false;
        this.diets = false;
        this.calendar = false;
        this.packages = !this.packages;
        break;
      }
    }
  }

  getUser(id: number): void{
    this.authService.getUser(id).subscribe((data) => {
      this.user = data;
    });
  }

  ngOnInit(): void {
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
      this.getUser(this.tokenIdInt);
    }
  }

}
