import { Component, Input, OnInit } from '@angular/core';
import { PhysiotherapistPackage } from 'src/app/_modles/physiotherapist';
import { PhysiotherapistEx } from 'src/app/_modles/physiotherapistEx';
import { PhysiotherapistExService } from 'src/app/_services/physiotherapist-ex.service';
import { PhysiotherapistPackageService } from 'src/app/_services/physiotherapist-package.service';

@Component({
  selector: 'app-medicine',
  templateUrl: './medicine.component.html',
  styleUrls: ['./medicine.component.sass'],
})
export class MedicineComponent implements OnInit {
  @Input() public physioPackageID: number;
  public physioPackage: PhysiotherapistPackage;
  public physioExs: PhysiotherapistEx[] = [];

  constructor(
    private physiotherapistPackageService: PhysiotherapistPackageService,
    private physiotherapistExService: PhysiotherapistExService
  ) {}

  getPhysioPackage(id: number): void {
    this.physiotherapistPackageService
      .getPackage(id)
      .subscribe((data) => (this.physioPackage = data));
  }

  getPhysioEx(): void {
    this.physiotherapistExService.getMedicalExs().subscribe((data) => {
      this.physioExs = data;
      this.physioExs = this.physioExs.filter(
        (p) => p.PhysiotherapistPackageId === this.physioPackageID
      );
    });
  }

  ngOnInit(): void {
    this.getPhysioPackage(this.physioPackageID);
    this.getPhysioEx();
  }
}
