import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef,
  OnInit,
  Input,
  OnChanges,
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  add,
} from 'date-fns';
import { Observable, Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';
import { Event } from 'src/app/_modles/event';
import { AuthService } from 'src/app/_services/-auth.service';
import { EventService } from 'src/app/_services/event.service';
import { AlertifyService } from 'src/app/_services/alertify.service';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

@Component({
  selector: 'app-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./calendar.component.sass'],
  templateUrl: './calendar.component.html',
})
export class CalendarComponent implements OnInit, OnChanges {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  @Input() public userID;
  @Input() public account;
  public token: any;
  public tokenId;
  public tokenIdInt: number;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: Event;
  };

  actions: CalendarEventAction[] = [
    // {
    //   label: '<i class="fas fa-fw fa-pencil-alt"></i>',
    //   a11yLabel: 'Edit',
    //   onClick: ({ event }: { event: Event }): void => {
    //     this.handleEvent('Edited', event);
    //   },
    // },
    // {
    //   label: '<i class="fas fa-fw fa-trash-alt"></i>',
    //   a11yLabel: 'Delete',
    //   onClick: ({ event }: { event: Event }): void => {
    //     this.event = this.event.filter((iEvent) => iEvent !== event);
    //     this.handleEvent('Deleted', event);
    //   },
    // },
  ];

  enable: any = [
    {
      from: '2010-04-01',
      to: '3025-05-01',
    },
  ];

  event: Event[];

  events: Event[] = [];

  activeDayIsOpen = true;

  refresh: Subject<any> = new Subject();

  constructor(
    private modal: NgbModal,
    private authService: AuthService,
    private eventService: EventService,
    private alertifyService: AlertifyService
  ) {}

  logged(): boolean {
    return this.authService.loggedIn();
  }

  ngOnChanges(): void {
    this.getEvents();
  }

  ngOnInit(): void {
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
    }
    this.getEvents();
  }
  getEvents(): void {
    this.eventService.getEvents().subscribe((data) => {
      data.forEach((element) => {
        element.start = new Date(element.start);
        element.end = new Date(element.end);
        element.color = {
          primary: element.primary,
          secondary: element.secondary,
        };
      });
      this.event = data;
      this.events = this.event.filter((e) => e.UserId === this.userID);
      this.refresh.next();
    });
  }

  updateEvent(event: Event): void {
    this.eventService
      .updateEvent({
        id: event.id,
        title: event.title,
        start: event.start,
        end: event.end,
        primary: event.color.primary,
        secondary: event.color.secondary,
      } as Event)
      .subscribe(() =>
        this.alertifyService.success('Wydarzenie zaktualizowano!')
      );
    this.refresh.next();
  }

  dayClicked({ date, events }: { date: Date; events: Event[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  // eventTimesChanged({
  //   event,
  //   newStart,
  //   newEnd,
  // }: CalendarEventTimesChangedEvent): void {
  //   this.events = this.events.map((iEvent) => {
  //     if (iEvent === event) {
  //       return {
  //         ...event,
  //         start: newStart,
  //         end: newEnd,
  //       };
  //     }
  //     return iEvent;
  //   });
  //   this.handleEvent('Dropped or resized', event);
  // }

  handleEvent(action: string, event: Event): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.eventService
      .addEvent({
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        title: 'New event',
        primary: '#ff0000',
        secondary: '#dd6e6e',
        UserId: this.userID,
      } as Event)
      .subscribe(() => this.getEvents());
  }

  deleteEvent(eventToDelete: Event): void {
    this.eventService.deleteEvent(eventToDelete).subscribe(() => {
      // this.events = this.events.filter((event) => event !== eventToDelete);
      this.getEvents();
      this.alertifyService.success('Wydarzenie usunięto!');
    });
  }

  setView(view: CalendarView): void {
    this.view = view;
  }

  closeOpenMonthViewDay(): void {
    this.activeDayIsOpen = false;
  }
}
