import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Diet } from 'src/app/_modles/diet';
import { DietPlan } from 'src/app/_modles/dietPlan';
import { DietProduct } from 'src/app/_modles/dietProduct';
import { User } from 'src/app/_modles/user';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { DietService } from 'src/app/_services/diet.service';

@Component({
  selector: 'app-diets',
  templateUrl: './diets.component.html',
  styleUrls: ['./diets.component.sass'],
})
export class DietsComponent implements OnInit {
  @Input() public userID: number;
  @Input() public coachID: number;
  @Input() public dietPlans: DietPlan[];
  public diets: Diet[];
  public dietProducts: DietProduct[];
  public addDietPlanForm = false;
  public addDietForm = false;
  public addProductForm = false;
  public editDiet = false;
  public editProduct = false;

  time = new FormControl('');

  dietId = new FormControl('');
  dietplan = new FormControl('');
  calorific = new FormControl('');
  dietname = new FormControl('');

  productId = new FormControl('');
  diet = new FormControl('');
  productname = new FormControl('');
  weight = new FormControl('');
  priceforweight = new FormControl('');
  price = new FormControl('');

  form = new FormGroup({
    time: this.time,
  });
  form2 = new FormGroup({
    dietId: this.dietId,
    dietplan: this.dietplan,
    dietname: this.dietname,
    calorific: this.calorific,
  });
  form3 = new FormGroup({
    productId: this.productId,
    productname: this.productname,
    weight: this.weight,
    priceforweight: this.priceforweight,
    diet: this.diet,
    // price: this.priceforweight,
  });

  constructor(
    private alertifyService: AlertifyService,
    private dietService: DietService
  ) {}

  showAddForm(id: number): void {
    switch (id) {
      case 1: {
        this.addDietPlanForm = !this.addDietPlanForm;
        this.addDietForm = false;
        this.addProductForm = false;
        break;
      }
      case 2: {
        if (this.editDiet) {
          this.editDiet = false;
        } else {
          this.addDietPlanForm = false;
          this.addDietForm = !this.addDietForm;
          this.addProductForm = false;
        }
        this.form2.reset();
        this.editProduct = false;
        break;
      }
      case 3: {
        if (this.editProduct) {
          this.editProduct = false;
        } else {
          this.addDietPlanForm = false;
          this.addDietForm = false;
          this.addProductForm = !this.addProductForm;
        }
        this.form3.reset();
        this.editProduct = false;
        break;
      }
      default: {
        this.addDietPlanForm = false;
        this.addDietForm = false;
        this.addProductForm = false;
        this.editDiet = false;
        this.editProduct = false;
        break;
      }
    }
  }

  showEditDietForm(diet: Diet): void {
    if (!this.addDietForm || this.editDiet) {
      this.addDietForm = !this.addDietForm;
    }
    this.editDiet = true;
    this.form2.setValue({
      dietId: diet.Id,
      dietplan: diet.DietPlanId,
      dietname: diet.DietName,
      calorific: diet.Calorific,
    });
  }
  showEditProductForm(product: DietProduct): void {
    if (!this.addProductForm || this.editProduct) {
      this.addProductForm = !this.addProductForm;
    }
    this.editProduct = true;
    this.form3.setValue({
      productId: product.Id,
      productname: product.ProductName,
      weight: product.Weight,
      priceforweight: product.PriceForWeight,
      diet: product.DietId,
    });
  }

  addPlan(): void {
    this.dietService
      .addDietPlan({
        Time: this.form.value.time.toString(),
        UserId: this.userID,
        CoachId: this.coachID,
      } as DietPlan)
      .subscribe((plan) => {
        this.dietPlans.push(plan);
        this.form.reset();
        this.showAddForm(0);
        this.alertifyService.success(
          'Plan dodany! Możesz dodać dietę do planu.'
        );
      });
  }

  addDiet(): void {
    this.dietService
      .addDiet({
        DietName: this.form2.value.dietname.toString(),
        Calorific: this.form2.value.calorific.toString(),
        DietPlanId: this.form2.value.dietplan,
      } as Diet)
      .subscribe((diet) => {
        this.diets.push(diet);
        this.form2.reset();
        this.showAddForm(0);
        this.alertifyService.success(
          'Dieta dodana! Możesz dodać produkty do diety.'
        );
      });
  }

  addProduct(): void {
    const w: number = +this.form3.value.weight;
    const p: number = +this.form3.value.priceforweight;
    const s: number = (w / 100) * p;
    const sum = parseFloat(s.toFixed(2));
    this.dietService
      .addProduct({
        ProductName: this.form3.value.productname.toString(),
        Weight: this.form3.value.weight,
        PriceForWeight: this.form3.value.priceforweight.toString(),
        DietId: this.form3.value.diet,
        Price: sum.toString(),
      } as DietProduct)
      .subscribe((product) => {
        this.dietProducts.push(product);
        this.form3.reset();
        this.showAddForm(0);
        this.alertifyService.success('Produkt dodany!');
      });
  }

  updateDiet(): void {
    this.dietService
      .updateDiet({
        Id: this.form2.value.dietId,
        DietName: this.form2.value.dietname,
        Calorific: this.form2.value.calorific.toString(),
        DietPlanId: this.form2.value.dietplan,
      } as Diet)
      .subscribe(() => {
        this.addDietForm = false;
        this.ngOnInit();
        this.alertifyService.success('Dieta zaktualizowana! Odśwież stronę.');
      });
  }
  updateProduct(): void {
    const w: number = +this.form3.value.weight;
    const p: number = +this.form3.value.priceforweight;
    const s: number = (w / 100) * p;
    const sum = parseFloat(s.toFixed(2));
    this.dietService
      .updateProduct({
        Id: this.form3.value.productId,
        ProductName: this.form3.value.productname,
        Weight: this.form3.value.weight.toString(),
        PriceForWeight: this.form3.value.priceforweight.toString(),
        Price: sum.toString(),
        DietId: this.form3.value.diet
      } as DietProduct)
      .subscribe(() => {
        this.addProductForm = false;
        this.ngOnInit();
        this.alertifyService.success('Produkt zaktualizowany! Odśwież stronę.');
      });
  }

  ngOnInit(): void {
    this.diets = [];
    if (this.dietPlans.length > 0) {
      this.dietPlans.forEach((element) => {
        if (element.Diets.length > 0) {
          element.Diets.forEach((dieta) => {
            this.diets.push(dieta);
          });
        }
      });
      this.diets.forEach((e) => {
        this.dietProducts = e.DietProducts;
      });
    }
  }
}
