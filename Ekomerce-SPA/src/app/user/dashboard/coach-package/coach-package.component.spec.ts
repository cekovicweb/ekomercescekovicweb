import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachPackageComponent } from './coach-package.component';

describe('CoachPackageComponent', () => {
  let component: CoachPackageComponent;
  let fixture: ComponentFixture<CoachPackageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoachPackageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoachPackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
