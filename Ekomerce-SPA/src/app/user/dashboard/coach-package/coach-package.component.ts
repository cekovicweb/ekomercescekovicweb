import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CoachPackage } from 'src/app/_modles/coachPackage';
import { DoctorEx } from 'src/app/_modles/doctorEx';
import { DoctorPackage } from 'src/app/_modles/doctorPackage';
import { PhysiotherapistPackage } from 'src/app/_modles/physiotherapist';
import { PhysiotherapistEx } from 'src/app/_modles/physiotherapistEx';
import { User } from 'src/app/_modles/user';
import { AuthService } from 'src/app/_services/-auth.service';
import { CoachPackageService } from 'src/app/_services/coach-package.service';
import { DoctorExService } from 'src/app/_services/doctor-ex.service';
import { DoctorPackageService } from 'src/app/_services/doctor-package.service';
import { PhysiotherapistExService } from 'src/app/_services/physiotherapist-ex.service';
import { PhysiotherapistPackageService } from 'src/app/_services/physiotherapist-package.service';

@Component({
  selector: 'app-coach-package',
  templateUrl: './coach-package.component.html',
  styleUrls: ['./coach-package.component.sass'],
})
export class CoachPackageComponent implements OnInit {
  @Input() user: User;
  public token: any;
  public tokenId: any;
  public tokenIdInt: number;
  public packages: CoachPackage[];
  public physioPackages: PhysiotherapistPackage[];
  public physioMedicalExs: PhysiotherapistEx[] = [];
  public doctorPackages: DoctorPackage[];
  public docotrMedicalExs: DoctorEx[] = [];
  public addPackageForm = false;
  public addMedicalExForm = false;
  public edit = false;
  public editMedicine = false;

  id = new FormControl('');
  package = new FormControl('', Validators.required);
  price = new FormControl('', Validators.required);
  coachId = new FormControl('');

  medicalExId = new FormControl('');
  medicalPackage = new FormControl('');
  medicalExName = new FormControl('');
  medicalExPrice = new FormControl('');

  form = new FormGroup({
    id: this.id,
    package: this.package,
    price: this.price,
    coachId: this.coachId,
  });

  form2 = new FormGroup({
    medicalExId: this.medicalExId,
    medicalPackage: this.medicalPackage,
    medicalExName: this.medicalExName,
    medicalExPrice: this.medicalExPrice
  });

  constructor(
    private coachPackageService: CoachPackageService,
    private physiotherapistPackageService: PhysiotherapistPackageService,
    private physiotherapistExService: PhysiotherapistExService,
    private doctorPackageService: DoctorPackageService,
    private doctorExService: DoctorExService,
    private authService: AuthService
  ) {}

  showAddPackageForm(): void {
    this.addPackageForm = !this.addPackageForm;
    this.addMedicalExForm = false;
    this.edit = false;
  }
  showAddMedicalExForm(): void {
    this.addMedicalExForm = !this.addMedicalExForm;
    this.addPackageForm = false;
    this.editMedicine = false;
    this.form2.reset();
  }

  showEditForm(coachPackage: any, account: string): void {
    this.edit = true;
    this.addPackageForm = !this.addPackageForm;
    if (account === 'trener'){
      this.form.setValue({
        id: coachPackage.Id,
        package: coachPackage.PackageName,
        price: coachPackage.Price,
        coachId: coachPackage.CoachId
      });
    } else if (account === 'Fizjoterapeuta'){
      this.form.setValue({
        id: coachPackage.Id,
        package: coachPackage.PackageName,
        price: coachPackage.Price,
        coachId: coachPackage.PhysiotherapistId
      });
    } else if (account === 'Lekarz sportowy'){
      this.form.setValue({
        id: coachPackage.Id,
        package: coachPackage.PackageName,
        price: coachPackage.Price,
        coachId: coachPackage.DoctorId
      });
    }
  }
  showEditExForm(physioEx: any, account: string): void {
    this.editMedicine = true;
    this.addMedicalExForm = !this.addMedicalExForm;
    if (account === 'Fizjoterapeuta'){
      this.form2.setValue({
        medicalExId: physioEx.Id,
        medicalPackage: physioEx.PhysiotherapistPackageId,
        medicalExName: physioEx.Name,
        medicalExPrice: physioEx.Price
      });
    } else if (account === 'Lekarz sportowy') {
      this.form2.setValue({
        medicalExId: physioEx.Id,
        medicalPackage: physioEx.DoctorPackageId,
        medicalExName: physioEx.Name,
        medicalExPrice: physioEx.Price
      });
    }
  }

  addPackage(account: string): void {

    if (account === 'trener'){
      this.coachPackageService
      .addPackage({
        PackageName: this.form.value.package.toString(),
        Price: this.form.value.price.toString(),
        CoachId: this.tokenIdInt,
      } as CoachPackage)
      .subscribe((coachPackage) => {
        this.packages.push(coachPackage);
      });
    } else if (account === 'Fizjoterapeuta'){
      this.physiotherapistPackageService
      .addPackage({
        PackageName: this.form.value.package.toString(),
        Price: this.form.value.price.toString(),
        PhysiotherapistId: this.tokenIdInt
      } as PhysiotherapistPackage)
      .subscribe((physioPackage) => {
        this.physioPackages.push(physioPackage);
      });
    } else if (account === 'Lekarz sportowy'){
      this.doctorPackageService
      .addPackage({
        PackageName: this.form.value.package.toString(),
        Price: this.form.value.price.toString(),
        DoctorId: this.tokenIdInt
      } as DoctorPackage)
      .subscribe((doctorPackage) => {
        this.doctorPackages.push(doctorPackage);
      });
    }

    this.form.reset();
    this.showAddPackageForm();
  }

  updatePackage(account: string): void {
    if (account === 'trener'){
      this.coachPackageService
      .updatePackage({
        Id: this.form.value.id,
        PackageName: this.form.value.package,
        Price: this.form.value.price,
        CoachId: this.form.value.coachId
      } as CoachPackage)
      .subscribe(() => this.getPackages(account));
    } else if (account === 'Fizjoterapeuta'){
      this.physiotherapistPackageService
      .updatePackage({
        Id: this.form.value.id,
        PackageName: this.form.value.package,
        Price: this.form.value.price,
        PhysiotherapistId: this.form.value.coachId
      } as PhysiotherapistPackage)
      .subscribe(() => this.getPackages(account));
    } else if (account === 'Lekarz sportowy'){
      this.doctorPackageService
      .updatePackage({
        Id: this.form.value.id,
        PackageName: this.form.value.package,
        Price: this.form.value.price,
        DoctorId: this.form.value.coachId
      } as DoctorPackage)
      .subscribe(() => this.getPackages(account));
    }
    this.addPackageForm = false;
  }

  addMedicalEx(account: string): void {
    if (account === 'Fizjoterapeuta'){
      this.physiotherapistExService
      .addMedicalEx({
        Name: this.form2.value.medicalExName,
        Price: this.form2.value.medicalExPrice,
        PhysiotherapistPackageId: this.form2.value.medicalPackage
      } as PhysiotherapistEx)
      .subscribe((physioEx) => {
        this.physioMedicalExs.push(physioEx);
        this.getPackages(this.user.AccountType);
      });
    } else if (account === 'Lekarz sportowy'){
      this.doctorExService
      .addMedicalEx({
        Name: this.form2.value.medicalExName,
        Price: this.form2.value.medicalExPrice,
        DoctorPackageId: this.form2.value.medicalPackage
      } as DoctorEx)
      .subscribe((doctorEx) => {
        this.docotrMedicalExs.push(doctorEx);
        this.getPackages(this.user.AccountType);
      });
    }

    this.form2.reset();
    this.showAddMedicalExForm();
  }

  updateMedicalEx(account: string): void {
    if (account === 'Fizjoterapeuta'){
      this.physiotherapistExService
      .updateMedicalEx({
        Id: this.form2.value.medicalExId,
        Name: this.form2.value.medicalExName,
        Price: this.form2.value.medicalExPrice,
        PhysiotherapistPackageId: this.form2.value.medicalPackage
      } as PhysiotherapistEx)
      .subscribe(() => this.getPackages(account));
    } else if (account === 'Lekarz sportowy'){
      this.doctorExService
      .updateMedicalEx({
        Id: this.form2.value.medicalExId,
        Name: this.form2.value.medicalExName,
        Price: this.form2.value.medicalExPrice,
        DoctorPackageId: this.form2.value.medicalPackage
      } as DoctorEx)
      .subscribe(() => this.getPackages(account));
    }
    this.addMedicalExForm = false;
  }

  deleteMedicalEx(medicalEx: PhysiotherapistEx): void {
    this.physiotherapistExService.deleteMedicineEx(medicalEx).subscribe(() => {
      // this.getPhysioMedicineEx(this.user.AccountType);
      this.ngOnInit();
    });
  }
  deleteDoctorMedicalEx(medicalEx: DoctorEx): void {
    this.doctorExService.deleteMedicineEx(medicalEx).subscribe(() => {
      // this.getPhysioMedicineEx(this.user.AccountType);
      this.ngOnInit();
    });
  }

  getPhysioMedicineEx(account: string): void {
    if (account === 'trener'){
      this.physiotherapistExService.getMedicalExs().subscribe((data) => {
        this.physioMedicalExs = data;
      });
    } else
    if (account === 'Lekarz sportowy'){
      this.doctorExService.getMedicalExs().subscribe((data) => {
        this.docotrMedicalExs = data;
      });
    }
  }

  getPackages(account: string): void {
    if (account === 'trener'){
      this.coachPackageService.getPackages().subscribe((data) => {
        this.packages = data;
        this.packages = this.packages.filter((coachPackage) => coachPackage.CoachId === this.tokenIdInt);
      });
    }
    else if (account === 'Fizjoterapeuta'){
      this.physiotherapistPackageService.getPackages().subscribe((data) => {
        this.physioPackages = data;
        this.physioPackages = this.physioPackages.filter((physioPackage) => physioPackage.PhysiotherapistId === this.tokenIdInt);
      });
    } else if (account === 'Lekarz sportowy'){
      this.doctorPackageService.getPackages().subscribe((data) => {
        this.doctorPackages = data;
        this.doctorPackages = this.doctorPackages.filter((doctorPackage) => doctorPackage.DoctorId === this.tokenIdInt);
      });
    }
  }

  findInArray(arr: Array<any>, name: string): boolean {
    const data = arr.some(el => el.PackageName === name);

    if (data){
      return true;
    }
    else{
      return false;
    }
  }

  logged(): boolean {
    return this.authService.loggedIn();
  }

  ngOnInit(): void {
    this.getPackages(this.user.AccountType);
    this.getPhysioMedicineEx(this.user.AccountType);
    if (this.logged()) {
      this.token = this.authService.getTokenId();
      this.tokenId = this.token.nameid;
      this.tokenIdInt = +this.tokenId;
    }
  }
}
