import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.sass'],
})
export class BarComponent implements OnInit {
  constructor() {}
  @Output() showProduct = new EventEmitter<void>();
  @Output() showSold = new EventEmitter<void>();
  @Output() showPackages = new EventEmitter<void>();
  @Output() showStorage = new EventEmitter<void>();
  @Output() showArticle = new EventEmitter<void>();
  @Output() showServices = new EventEmitter<void>();
  @Output() showDiets = new EventEmitter<void>();
  @Output() showWorkouts = new EventEmitter<void>();
  @Output() showMedicine = new EventEmitter<void>();
  // @Output() showDashboard = new EventEmitter<void>();
  @Input() public account;
  @Input() public coachPackage;
  @Input() public physiotherapistPackage;
  @Input() public doctorPackage;
  ngOnInit(): void {
  }
}
