import { Component, Input, OnInit } from '@angular/core';
import { Diet } from 'src/app/_modles/diet';
import { DietPlan } from 'src/app/_modles/dietPlan';
import { DietProduct } from 'src/app/_modles/dietProduct';
import { DietService } from 'src/app/_services/diet.service';

@Component({
  selector: 'app-user-diets',
  templateUrl: './user-diets.component.html',
  styleUrls: ['./user-diets.component.sass']
})
export class UserDietsComponent implements OnInit {
  @Input() public userID;
  public dietPlans: DietPlan[];
  public diets: Diet[];
  public dietProducts: DietProduct[];


  constructor(private dietService: DietService) { }

  getDiets(): void {
    this.diets = [];
    this.dietService.getDietPlans().subscribe((data) => {
      this.dietPlans = data;
      this.dietPlans = this.dietPlans.filter(item => item.UserId === this.userID);
      if (data.length > 0) {
        data.forEach((el) => {
          if (el.UserId === this.userID){
          if (el.Diets.length > 0){
            el.Diets.forEach((tr) => {
              this.diets.push(tr);
            });
          }
        }});
        this.diets.forEach((e) => {
          this.dietProducts = e.DietProducts;
        });
      }
    });
  }

  ngOnInit(): void {
    this.getDiets();
  }

}
