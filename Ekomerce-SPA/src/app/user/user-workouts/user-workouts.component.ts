import { Component, Input, OnInit } from '@angular/core';
import { CoachPackage } from 'src/app/_modles/coachPackage';
import { Training } from 'src/app/_modles/training';
import { TrainingExercise } from 'src/app/_modles/trainingExercise';
import { TrainingPlan } from 'src/app/_modles/trainingPlan';
import { CoachPackageService } from 'src/app/_services/coach-package.service';
import { TrainingService } from 'src/app/_services/training.service';

@Component({
  selector: 'app-user-workouts',
  templateUrl: './user-workouts.component.html',
  styleUrls: ['./user-workouts.component.sass'],
})
export class UserWorkoutsComponent implements OnInit {
  @Input() public userID;
  public trainingPlans: TrainingPlan[];
  public trainings: Training[];
  public trainingExercises: TrainingExercise[];

  constructor(private trainingService: TrainingService) {}

  getWorkouts(): void {
    this.trainings = [];
    this.trainingService.getTrainingPlans().subscribe((data) => {
      this.trainingPlans = data;
      this.trainingPlans = this.trainingPlans.filter(item => item.UserId === this.userID);
      if (data.length > 0) {
        data.forEach((el) => {
          if (el.UserId === this.userID){
          if (el.Trainings.length > 0){
            el.Trainings.forEach((tr) => {
              this.trainings.push(tr);
            });
          }
        }});
        this.trainings.forEach((e) => {
          this.trainingExercises = e.TrainingExercises;
        });
      }
    });
  }

  ngOnInit(): void {
    this.getWorkouts();
  }
}
