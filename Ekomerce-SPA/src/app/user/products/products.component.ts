import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Category } from 'src/app/_modles/category';
import { Product } from 'src/app/_modles/product';
import { AuthService } from 'src/app/_services/-auth.service';
import { CategoryService } from 'src/app/_services/category.service';
import { ImageService } from 'src/app/_services/image.service';
import { ProductsService } from '../../_services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass'],
})
export class ProductsComponent implements OnInit {
  products = [];
  categorys: Category[];
  public token: any = this.auth.getTokenId();
  public tokenId = this.token.nameid;
  public tokenIdInt: number = +this.tokenId;

  constructor(
    private productService: ProductsService,
    private categoryService: CategoryService,
    private imageService: ImageService,
    private auth: AuthService
  ) {}

  @Input() public button1: boolean;
  public fileTypeCheck = false;
  public addProductForm = false;
  public isMainChecbox = false;
  public file;

  ngOnInit(): void {
    this.get();
    this.getCategorys();
  }

  // method to validate form input file
  onFileChange(fileEvent): any[] {
    const file = fileEvent.target.files[0];
    const types = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];

    const fileType = file.type;
    let check: boolean;
    for (const type of types) {
      if (fileType === type) {
        check = true;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid #00dc00';
        return [(this.fileTypeCheck = check), (this.file = file)];
      } else {
        check = false;
        document.getElementById('imageUrl').style.borderBottom =
          '2px solid red';
      }
    }

    return [(this.fileTypeCheck = check)];
  }

  // show/hide form to add new product
  showAddProductForm(): void {
    this.addProductForm = !this.addProductForm;
  }

  getCheckboxValue(e): void {
    this.isMainChecbox = e.target.checked;
  }

  get(): void {
    this.products = [];
    this.productService.getProduct().subscribe((data) => {
      for (const prod of data) {
        if (prod.UserId === this.tokenIdInt) {
          this.products.push(prod);
        }
      }
    });
  }

  add(
    name: string,
    description: string,
    price: string,
    category: number,
    form: NgForm
  ): void{
    const formData = new FormData();
    formData.append('file', this.file, this.file.name);
    this.imageService.addImage(formData).subscribe((data) => {
      const cat = category;
      const catInt: number = +cat;
      this.productService
        .addProduct({
          Name: name.toString(),
          Description: description.toString(),
          Price: price.toString(),
          IsMain: this.isMainChecbox,
          CategoryId: catInt,
          UserId: this.tokenIdInt,
          ImageId: data.Id,
        } as Product)
        .subscribe((product) => this.updateAdd(product));
      form.reset();
      this.showAddProductForm();
    });
  }

  updateAdd(product): void {
    this.products.push(product);
    this.ngOnInit();
  }

  delete(product: Product): void {
    this.products = this.products.filter((p) => p.Id !== product.Id);
    this.productService.deleteProduct(product).subscribe();
    this.imageService.deleteImage(product.Image).subscribe();
  }

  getCategorys(): any {
    this.categoryService
      .getCategorys()
      .subscribe((data) => (this.categorys = data));
  }

  priceToFloat(price): number {
    price.replace(/,/g, '.');
    return price.replace(/,/g, '.');
  }
}
