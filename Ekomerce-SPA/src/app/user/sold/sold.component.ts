import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sold',
  templateUrl: './sold.component.html',
  styleUrls: ['./sold.component.sass']
})
export class SoldComponent implements OnInit {

  constructor() { }
  @Input() public button2: boolean;
  ngOnInit(): void {
  }

}
