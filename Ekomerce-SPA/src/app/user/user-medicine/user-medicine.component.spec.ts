import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMedicineComponent } from './user-medicine.component';

describe('UserMedicineComponent', () => {
  let component: UserMedicineComponent;
  let fixture: ComponentFixture<UserMedicineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserMedicineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMedicineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
