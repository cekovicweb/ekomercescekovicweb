import { Component, Input, OnInit } from '@angular/core';
import { DoctorEx } from 'src/app/_modles/doctorEx';
import { DoctorPackage } from 'src/app/_modles/doctorPackage';
import { PhysiotherapistPackage } from 'src/app/_modles/physiotherapist';
import { PhysiotherapistEx } from 'src/app/_modles/physiotherapistEx';
import { DoctorExService } from 'src/app/_services/doctor-ex.service';
import { DoctorPackageService } from 'src/app/_services/doctor-package.service';
import { PhysiotherapistExService } from 'src/app/_services/physiotherapist-ex.service';
import { PhysiotherapistPackageService } from 'src/app/_services/physiotherapist-package.service';

@Component({
  selector: 'app-user-medicine',
  templateUrl: './user-medicine.component.html',
  styleUrls: ['./user-medicine.component.sass'],
})
export class UserMedicineComponent implements OnInit {
  @Input() public userID: number;
  @Input() public physioPackageID: number;
  @Input() public doctorPackageID: number;
  public physioPackage: PhysiotherapistPackage;
  public physioExs: PhysiotherapistEx[] = [];
  public doctorPackage: DoctorPackage;
  public doctorExs: DoctorEx[] = [];

  constructor(
    private physiotherapistPackageService: PhysiotherapistPackageService,
    private physiotherapistExService: PhysiotherapistExService,
    private doctorPackageService: DoctorPackageService,
    private doctorExService: DoctorExService,
  ) {}

  getPhysioPackage(id: number): void {
    this.physiotherapistPackageService
      .getPackage(id)
      .subscribe((data) => (this.physioPackage = data));
  }

  getPhysioEx(): void {
    this.physiotherapistExService.getMedicalExs().subscribe((data) => {
      this.physioExs = data;
      this.physioExs = this.physioExs.filter(
        (p) => p.PhysiotherapistPackageId === this.physioPackageID
      );
    });
  }
  getDoctorPackage(id: number): void {
    this.doctorPackageService
      .getPackage(id)
      .subscribe((data) => (this.doctorPackage = data));
  }

  getDoctorEx(): void {
    this.doctorExService.getMedicalExs().subscribe((data) => {
      this.doctorExs = data;
      this.doctorExs = this.doctorExs.filter(
        (p) => p.DoctorPackageId === this.doctorPackageID
      );
    });
  }

  ngOnInit(): void {
    if (this.physioPackageID !== null) {
      this.getPhysioPackage(this.physioPackageID);
      this.getPhysioEx();
    }
    if (this.doctorPackageID !== null) {
      this.getDoctorPackage(this.doctorPackageID);
      this.getDoctorEx();
    }
  }
}
