import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { Switch, Route, BrowserRouter, Link } from 'react-router-dom';
import { BuilderComponent } from '@builder.io/react';

import './index.css';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route render={({ location }) => <CatchallPage key={location.key} />} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

// class CatchallPage extends React.Component {
  const CatchallPage = () => {
  // state = { notFound: false };
    const [data, setData] = useState(false);
    const [apiKey, setApiKey] = useState('ApiKey')


    const handleSetApi = e => {
      localStorage.setItem('apiBuilder', e.target.value)

      setApiKey(e.target.value)
    }
  
    return !data ? (

      // f08e368fc1364eca9db3f179c5aaf7a6
      <BuilderComponent
        apiKey={localStorage.getItem('apiBuilder')}
        model="page"
        contentLoaded={content => {
          if (!content) {
            setData(true);
          }
        }}
      >
        <div className="loading">Loading...
        <input type="text" value={apiKey} onChange={handleSetApi}/>
        </div>
      </BuilderComponent>
    ) : (
      <input type="text" value={apiKey} onChange={handleSetApi}/> // Your 404 content
      
    );
  }


export default App

const NotFound = () => <h1>No page found for this URL, did you publish it?</h1>;